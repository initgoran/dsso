#!/usr/bin/perl

use strict;
use warnings;

use JSON::PP;
use SpellSet;

open(my $dssodb, "<", $ARGV[0])
    or die("Usage: $0 DB [ TAGS ]");

my %dict;

sub default_compound {
    my ($syu, $sfl) = @_;

    if (length($sfl) > 2) {
        return (substr($sfl, 2, 1) eq "y");
    }

    my $minlength;
    my $gc = $syu->{gc};
    if ($gc eq "substantiv") {
        $minlength = 4;
    } elsif ($gc eq "verb" or $gc eq "adjektiv") {
        $minlength = 5;
    } else {
        return 0;
    }

    foreach my $wf (@{$syu->{wfs}}) {
        if (length($wf->{w}) < $minlength or
            index($wf->{w}, " ") >= 0) {
            return 0;
        }
    }
    return 1;
}

sub has_tag {
    my ($obj, $tag) = @_;
    return 0 unless exists($obj->{tags}) && $obj->{tags};
    foreach my $t (split(",", $obj->{tags})) {
        return 1 if $t eq $tag;
    }
    return 0;
}

sub wordforms {
    my ($syu, $exclude_tags) = @_;

    my $cat = "normal";

    my $syutags = "";
    if (exists($syu->{tags}) && $syu->{tags}) {
        foreach my $tag (split(",", $syu->{tags})) {
            if ($tag eq "nocompsug" or $tag eq "obscene") {
                $cat = "nosuggest";
            }
            return if exists($exclude_tags->{$tag});
        }
    }

    my $default_nok = 0;
    
    my $sfl = "";
    if (exists($syu->{spell_flags})) {
        $sfl = $syu->{spell_flags};
        my $s = substr($sfl, 0, 1);
        if ($s eq "n") {
            $default_nok = 1;
        } elsif ($s eq "b") {
            $cat = "nosuggest";
        }
    }

    my $default_cok = default_compound($syu, $sfl);

    if (exists($syu->{spell_cp}) && $syu->{spell_cp}) {
        $dict{$cat}->add("prefix", split(",", $syu->{spell_cp}))
    }

    if (exists($syu->{spell_cm})) {
        $dict{$cat}->add("middle", split(",", $syu->{spell_cm}))
    }

    if (exists($syu->{bw}) && $syu->{bw}) {
        foreach my $word (split(",", $syu->{bw})) {
            if (exists($dict{$cat}{middle}{$word})) {
                $dict{$cat}->add("cok", $word . "-");
            } else {
                $dict{$cat}->add("cnok", $word . "-");
            }
        }
    }

    my $gc = $syu->{gc};
    if (!$sfl && $gc eq "prefix") {
        foreach my $wf (@{$syu->{wfs}}) {
            $dict{$cat}->add("cnok", $wf->{w} . "-");
        }
        return;
    }

    if ($gc eq "verb") {
        if (has_tag($syu, "vsms")) {
            $cat = "verb";
        } elsif ($cat eq "normal" && !$default_cok &&
                 !has_tag($syu, "nosms")) {
            my $longword = 1;
            foreach my $wf (@{$syu->{wfs}}) {
                if (length($wf->{w}) < 5) {
                    $longword = 0;
                    last;
                }
            }
            if ($longword) {
                $cat = "verb";
            }
        }
    }

    if ($gc eq "adjektiv") {
        if (has_tag($syu, "asms")) {
            $cat = "adjektiv";
        } elsif ($cat eq "normal" && !$default_cok &&
                 !has_tag($syu, "nosms")) {
            my $longword = 1;
            foreach my $wf (@{$syu->{wfs}}) {
                if (length($wf->{w}) < 5) {
                    $longword = 0;
                    last;
                }
            }
            if ($longword) {
                $cat = "adjektiv";
            }
        }
    }

    foreach my $wf (@{$syu->{wfs}}) {
        my $w = $wf->{w};
        if ($default_nok) {
            if ($default_cok || has_tag($wf, "cok")) {
                $dict{$cat}->add("suffix", $w);
            }
        } elsif (index($w, " ") >= 0) {
            $dict{$cat}->add("cnok", split(" ", $w));            
        } elsif (has_tag($wf, "nospell")) {
        } elsif ($default_cok) {
            if (has_tag($wf, "cnok")) {
                $dict{$cat}->add("cnok", $w);
            } else {
                $dict{$cat}->add("cok", $w);
            }
        } else {
            if (has_tag($wf, "cok")) {
                $dict{$cat}->add("cok", $w);
            } else {
                $dict{$cat}->add("cnok", $w);
            }
        }
    }
}

foreach my $cat (qw/normal verb adjektiv nosuggest/) {
    $dict{$cat} = new SpellSet();
}

my $etaglist = @ARGV > 1 ? $ARGV[1] : "sv_FI,kontrollera,redundant,ovanlig";

my %exclude = ();
foreach my $tag (split(",", $etaglist)) {
    undef($exclude{$tag});
}

my $grammar = <$dssodb>;
die unless defined($grammar);

my @db;
while (defined(my $line = <$dssodb>)) {
    my $syu = decode_json($line);
    die("Expected JSON: $line") unless ref($syu) && exists($syu->{id});
    $db[$syu->{id}] = $syu;
}

foreach my $syu (@db) {
    next unless $syu;
    if (exists($syu->{type}) && $syu->{type} eq "syu" &&
        exists($syu->{wfs})) {
        wordforms($syu, \%exclude);
    }
}

chdir("build");
foreach my $dir (keys(%dict)) {
    -d $dir || mkdir($dir);    
    $dict{$dir}->store_all($dir);
}
