BDIR:=$(DICTDIR)/build

XD_DB:=$(DICTDIR)/dsso_db.txt

XDCNOK:=$(BDIR)/normal/word_cnok.txt
XDPWORDS:=$(BDIR)/normal/word_pre.txt 
XDSWORDS:=$(BDIR)/normal/word_suf.txt

MOREWORDS:=$(BDIR)/morewords.txt

DEVEL_DICT:=$(DICTDIR)/resources/devel.dic
DEVEL_AFF:=$(DICTDIR)/resources/devel.aff

export PERL_HASH_SEED_DEBUG:=1
export PERL_PERTURB_KEYS:=0
export PERL_HASH_SEED:=0xf9f1b4c951016730

PWORDS:=words/pwords.txt words/word_p_words.txt

ADD_COLON_WORD:=words/abbreviations_colon.txt
ADD_DASH_WORD:=words/blocked_pwords.txt
LCNOKWORDS:=words/abbreviations_s.txt words/dashwords.txt \
	words/word_p_words.txt \
        words/otherwords.txt words/goodwords.txt
#MISSPELLED:=words/forbiddenwords.txt
EXTRA_DICT:=words/extra_dict.dic
MUNCH:=perl -Iperl prog/munch.pl

XD_DICTS:=$(XDCNOK) $(XDPWORDS)

# comma-separated list of tags. All words with any of those tags will be excluded:
#EXCLUDE_TAGS=sv_FI,kontrollera
EXCLUDE_TAGS=sv_FI,kontrollera,redundant,ovanlig
sv_FI.dic: EXCLUDE_TAGS=kontrollera,webb,redundant,ovanlig

all: $(BDIR)/sv_SE.dic
sv_SE: $(BDIR)/sv_SE.dic
sv_FI: $(BDIR)/sv_FI.dic 

$(DEVEL_DICT): $(BDIR)/sv_SE.dic
	cp -p $< $@

$(DEVEL_AFF): $(BDIR)/sv_SE.aff
	cp -p $< $@

install: $(DEVEL_DICT) $(DEVEL_AFF)

$(MOREWORDS): $(ADD_DASH_WORD) $(ADD_COLON_WORD) $(LCNOKWORDS)
	mkdir -p $(BDIR)/forbidden
	sed 's/\([^-]\)$$/\1-/' $(ADD_DASH_WORD) > $@
	perl -pe 's/(.*)/$$1\n$$1-\n$$1:s/' $(ADD_COLON_WORD) >> $@
	cat $(LCNOKWORDS) >> $@


$(XD_DICTS): $(XD_DB) $(PWORDS) $(MOREWORDS)
	cat $(PWORDS) >> $(XDPWORDS)
	sed 's/\([^-]\)$$/\1-/' $(ADD_COLON_WORD) >> $(XDPWORDS)
	cat $(MOREWORDS) >> $(XDCNOK)
	cat words/forbidden_cnok.txt >> $(BDIR)/forbidden/word_cnok.txt
	cat words/forbidden_cok.txt >> $(BDIR)/forbidden/word_cok.txt
	touch $(BDIR)/forbidden/word_pre.txt
	touch $(BDIR)/forbidden/word_mid.txt
	touch $(BDIR)/forbidden/word_suf.txt
	cat words/otherwords_nosug.txt >> $(BDIR)/nosuggest/word_cnok.txt
	#sed 's/\([^-]\)$$/\1-/' words/abbreviations_p.txt >> $(BDIR)/nosuggest/word_pre.txt
	cat words/abbreviations.txt words/abbreviations_p.txt >> $(XDCNOK)
	sed 's/\([^-]\)$$/\1-/' words/abbreviations_p.txt >> $(XDPWORDS)
	sed 's/^\([^-]\)/-\1/' words/abbreviations_s.txt >> $(XDSWORDS)

$(BDIR)/sv_SE.dic: $(BDIR)/sv_SE.aff $(XD_DICTS) $(EXTRA_DICT)
	$(MUNCH) sv_SE.aff $(BDIR) $(BDIR)/nosuggest $(BDIR)/forbidden $(EXTRA_DICT) $(IGNORE_WORDS) $@ || rm -f $@

# You should do a clean before (and after) making this target...
$(BDIR)/sv_FI.dic: $(BDIR)/sv_FI.aff $(XD_DICTS) $(EXTRA_DICT)
	cat words/otherwords_fi.txt >> $(XDCNOK)
	$(MUNCH) $(BDIR)/sv_FI.aff $(BDIR) $(BDIR)/nosuggest $(BDIR)/forbidden $(EXTRA_DICT) $(IGNORE_WORDS) $@ || rm -f $@

clean:
	$(RM) -r $(MOREWORDS) $(BDIR)/verb $(BDIR)/nosuggest $(BDIR)/adjektiv $(BDIR)/forbidden $(BDIR)/normal

$(BDIR)/sv_SE.aff: sv_SE.aff
	cp -p $< $@

$(BDIR)/sv_FI.aff: sv_SE.aff
	sed -e 's/^MAXDIFF .*/MAXDIFF 5/' -e 's/^MAXCPDSUGS .*/MAXCPDSUGS 2/' $< > $@

.PHONY: clean install all sv_SE sv_FI
