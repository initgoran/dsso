package SpellSet;

use strict;
use warnings;
use locale;
use POSIX qw(locale_h);

sub new {
    my ($pkg) = @_;
    my $self = {
		 cok => {},
		 cnok => {},
		 prefix => {},
		 middle => {},
		 suffix => {},
    };
    bless($self, $pkg);
    return $self;
}

sub merge {
    my ($self, $other) = @_;
    while ( my ($type, $ws) = each(%$other) ) {
	undef($self->{$type}{$_}) for (keys(%$ws));
    }
}

sub filenames {
    my ($pkg, $dir) = @_;
    my @filename = qw/word_cok.txt word_cnok.txt word_pre.txt
                      word_mid.txt word_suf.txt/;
    my @files = map { $dir . "/" . $_  } @filename;
    return @files;;
}

sub store_all {
    my ($self, $dir) = @_;
    mkdir($dir);
    $self->store($self->filenames($dir));
}

sub store {
    my ($self) = shift;
    foreach my $type (qw/cok cnok prefix middle suffix/) {
	my $d = $self->{$type};
	delete($d->{"!"});
	delete($d->{""});
	my $filename = shift;
	my $utfil;
	open($utfil, ">:encoding(utf-8)", $filename) and
	    print $utfil ("0\n",join("\n",sort(keys(%$d))), "\n")
	    and close($utfil) or die("cannot write file <$filename>");
    }
}

sub add {
    my $self = shift;
    my $cat = shift;
    foreach my $word (@_) {
	undef($self->{$cat}{$word});
    }
}

sub add_cok {
    my $self = shift;
    $self->add("cok", @_);
}

sub add_cnok {
    my $self = shift;
    $self->add("cnok", @_);
}

sub add_prefix {
    my $self = shift;
    $self->add("prefix", @_);
}

sub add_middle {
    my $self = shift;
    $self->add("middle", @_);
}

sub add_suffix {
    my $self = shift;
    $self->add("suffix", @_);
}

1;
