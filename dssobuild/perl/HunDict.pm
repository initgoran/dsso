package HunDict;

use locale;
use strict;

# Format: ref to hash; root words are key, flags are values.
# Undefined value means no flags.
# Note: each root word may appear only once, i.e. it may only
# have one set of flags.

sub new {
    my ($pkg, $dictfilename, $onlyincompound_flag, $no_merge_flags) = @_;
    my %words = ();
    my $self = \%words;
    bless($self, $pkg);
    defined($dictfilename) && length($dictfilename) or
        return $self;
    open(my $dict, "<:encoding(utf-8)", $dictfilename)
        or die("$dictfilename: cannot open: $!");
    while (defined(my $line = <$dict>)) {
        # First line may be a line count, just skip it:
        if ($. == 1 and $line =~ m/^\d+$/) {
            next;
        }
        chomp($line);
        my ($hw, $flags) = split("/", $line);
	$self->merge_word($hw, $flags, $onlyincompound_flag, $no_merge_flags)
    }
    close($dict)
        or die("cannot read $dictfilename: $!");
    return $self;
}

sub have_common_flag {
    my ($flags, $other) = @_;
    return 0 unless length($other);
    foreach my $f (split('', $flags)) {
	return 1 if index($other, $f) >= 0;
    }
    return 0;
}

sub merge_word {
    my ($self, $word, $flags, $onlyincompound_flag, $no_merge_flags) = @_;
    return unless defined($word) && length($word);
    if ($word eq '__HOMONYMS__') {
	exists($self->{__HOMONYMS__}) or $self->{__HOMONYMS__} = {};
	my $homs = $self->{__HOMONYMS__};
	while (my ($w, $flist) = each(%$flags)) {
	    #if (exists($homs->{$w})) {
		push(@{$homs->{$w}}, @$flist);
	    #} else {
	#	$homs->{$w} = [$flist];
	 #   }
	}
	return;
    }
    if (exists($self->{$word})) {
	my $oldflags = defined($self->{$word}) ? $self->{$word} : "";
	my $s = (defined($flags) ? $flags : "") . $oldflags;
	if (have_common_flag($s, $no_merge_flags)) {
	    if (defined($self->{$word}) and defined($flags)) {
		foreach my $f (split('', $flags)) {
		#    $oldflags =~ s/$f//;
		    my $i = index($oldflags, $f);
		    if ($i >= 0) {
			substr($oldflags, $i, 1) = "";
		    }
		}
		if (!length($oldflags)) {
		    $self->{$word} = $flags;
		    warn("No homonym: $word/$oldflags+$flags <--> $word/$flags");
		} else {
		    $oldflags = defined($self->{$word}) ? $self->{$word} : "";
		    warn("Homonym: $word/$flags+$self->{$word} --> $word/$flags+$oldflags");
		    push(@{$self->{__HOMONYMS__}{$word}}, $flags);
		    $self->{$word} = $oldflags;
		}
	    } else {
		$self->{$word} = $s;
		warn("No homonym: $word/$oldflags+$flags <--> $word/$s");
	    }
	} elsif (defined($self->{$word}) && defined($flags)) {
	    foreach my $i (split('', $flags)) {
		$self->{$word} .= $i
		    unless(index($oldflags, $i) >= 0);
	    }
	    if (defined($onlyincompound_flag) and
		index($oldflags, $onlyincompound_flag)<0 ||
		index($flags, $onlyincompound_flag)<0) {
		# This is a valid word, don't merge in the ONLYINCOMPOUND flag:
		$self->{$word} =~ s/$onlyincompound_flag//;
	    }
	    warn("warning: merging \"$word\": $oldflags+$flags=$self->{$word}");
	} else {
	    $self->{$word} .= $flags if defined($flags);
	    $self->{$word} =~ s/$onlyincompound_flag//
		if defined($self->{$word}) && defined($onlyincompound_flag);
	}	
    } else {
	$self->{$word} = $flags;
    }
}

sub merge {
    my ($self, $other, $onlyincompound_flag, $no_merge_flags) = @_;
    ref($other) or $other = new HunDict($other);
    while (my ($word, $flags) = each(%$other)) {
	$self->merge_word($word, $flags, $onlyincompound_flag, $no_merge_flags);
    }
}

sub size {
    my ($self) = @_;
    if (exists($self->{__HOMONYMS__})) {
	return scalar(keys(%$self))+scalar(keys(%{$self->{__HOMONYMS__}}))-1;
    } else {
	return scalar(keys(%$self));
    }
}

sub print {
    my ($self, $file) = @_;
    my $fh = \*STDOUT;
    if (defined($file)) {
	if (open(my $fhtmp, ">:utf8", $file)) {
	    $fh = $fhtmp;
	} else {
	    warn("cannot write to $file: $!");
	}
    }
    print $fh ($self->size(), "\n" );
    my $homs = exists($self->{__HOMONYMS__}) ? $self->{__HOMONYMS__} : {};
    foreach my $word (sort(keys(%$self))) {
	next if $word eq '__HOMONYMS__';
	if (exists($homs->{$word})) {
	    foreach my $flags (@{$homs->{$word}}) {
		if (defined($flags)) {
		    my $f = join('', sort(split('', $flags)));
		    print $fh ("$word/$f\n");
		} else {
		    print $fh ("$word\n");
		}
	    }
	}
	if (defined($self->{$word})) {
	    my $f = join('', sort(split('', $self->{$word})));
	    print $fh ("$word/$f\n");
	} else {
	    print $fh ("$word\n");
	}
    }
}

1;
