# dsso

 Swedish spell-check dictionary.

 To build the Swedish Hunspell dictionary, run GNU make in
 the project's top-level directory. The dictionary will be
 saved as sv_SE.aff and sv_SE.dic in the "build" directory.
 For this to work, a lot of dependencies must be installed.
 Alternatively, if you have installed Vagrant (see vagrantup.com),
 open a terminal window, cd to the same directory as this file and run
     vagrant up
 then a virtual machine will be installed to build the dictionary.

 The latest official build can be found in the resources directory.

# Mozilla and LibreOffice extensions

 To build extensions for Mozilla Firefox and LibreOffice,
 first build the dictionary for locales sv_SE and sv_FI:

    make
    make sv_FI

 and then run

    make oodict
    make ffdict

# Dependencies

To build the hunspell dictionary (sv_SE.dic and sv_FI.dic), the below tools are required:

* GNU Make

* Unix command line tools

* Perl

# Modifying the dictionary

 To modify the Swedish Hunspell dictionary, you should edit the source
 file dsso_db.txt and/ord the simple word lists in the dssobuild/words
 directory. The recommended method to edit dsso_db.txt is to build
 and use the gcorpus app in the src directory. Unfortunately, the C++
 code can only be compiled on Linux, and the below tools are required:

* Hunspell version 1.7

* Gtk+ and gtkmm version 3

* JsonCpp

# Build in a virtual machine

To try out the gcorpus application (or the other C++ tools) from a non-Linux system,
a virtual machine may be used.  Install Vagrant (https://www.vagrantup.com/) and run
the command `vagrant up` from the current directory (where the file `Vagrantfile` is
stored) to download and start the virtual machine.