all: wordlists
	$(MAKE) DICTDIR=$(shell pwd) -C dssobuild

sv_FI: wordlists_fi
	$(MAKE) DICTDIR=$(shell pwd) -C dssobuild $@

oodict:
	$(MAKE) -C oodict

ffdict:
	$(MAKE) -C ffdict

clean:
	$(MAKE) -C src clean

wordlists:
	mkdir -p build
	$(RM) -r build/[!s]*
	perl -Idssobuild/perl dssobuild/prog/gen_wordlists.pl dsso_db.txt \
		sv_FI,kontrollera,redundant,ovanlig

wordlists_fi:
	mkdir -p build
	$(RM) -r build/[!s]*
	perl -Idssobuild/perl dssobuild/prog/gen_wordlists.pl dsso_db.txt \
		kontrollera,webb,redundant,ovanlig

wordlists_c: index/stringdb.bin
	$(MAKE) -C src gen_dicts
	./src/gen_dicts

index/stringdb.bin:
	$(MAKE) -C src rebuild_dssodb
	mkdir -p index
	./src/rebuild_dssodb

.PHONY: all wordlists oodict ffdict
