#ifndef __SYUGETWORDFORMS_h__
#define __SYUGETWORDFORMS_h__

#include <iostream>
#include <set>

#include <jsoncpp/json/json.h>

#include "StringDB.h"

class SyuGetWords {
 public:
 SyuGetWords() : csdb(StringDB::get_db()) {}
  const std::set<unsigned int> &operator()(unsigned int id, const char *str) {
    static std::set<unsigned int> res;
    res.clear();
    if (id == 0) //Grammar
      return res;
    Json::Value syu;
    if (!_reader.parse(str, syu)) {
      //throw BadObjDB(std::string("bad JSON: ")+obj_str);
      return res;
    }

    Json::Value wfs = syu["wfs"];
    //std::cerr << "No wfs: " << wfs.size() << std::endl;

    for (unsigned int i=0; i < wfs.size(); ++i) {
      std::string wf =  wfs[i]["w"].asString();
      if (!wf.length())
	continue;
      unsigned int sno = csdb.find_or_add(wf);
      //std::cerr << "Word: " << wf << ", sno=" << sno << std::endl;
      if (sno)
	res.insert(sno);
      //std::cerr << "Wfs " << i << ": " << wf << ", sno " << sno << std::endl;
    }

    static const char *attname[3] = { "spell_cp", "spell_cm", "bw" };
    std::string prefs;
    for (unsigned int i=0; i<sizeof(attname)/sizeof(char *); ++i) {
      if (syu.isMember(attname[i])) {
	(prefs += syu[attname[i]].asString()) += ",";
      }
    }
    std::string::size_type oldpos = 0;
    while (oldpos < prefs.length()) {
      std::string::size_type newpos = prefs.find(",", oldpos);
      std::string word = prefs.substr(oldpos, newpos-oldpos);
      if (word.size()) {
	unsigned int sno = csdb.find_or_add(word);
	if (sno)
	  res.insert(sno);
      }
      oldpos = newpos+1;
    }
    /*
    std::cerr << "ID " << id << ", words: ";
    for (std::set<unsigned int>::const_iterator it = res.begin();
	 it != res.end(); ++it)
      std::cerr << *it << ' ';
    std::cerr << std::endl;
    */
    return res;
  }
 private:
  StringDB &csdb;
  Json::Reader _reader;
};

/*
class SyuGetWordforms {
 public:
 SyuGetWordforms() : csdb(StringDB::get_db()) {}
  const std::set<unsigned int> &operator()(unsigned int id, const char *str) {
    static std::set<unsigned int> res;
    res.clear();
    if (id == 0) //Grammar
      return res;
    Json::Value syu;
    if (!_reader.parse(str, syu)) {
      //throw BadObjDB(std::string("bad JSON: ")+obj_str);
      return res;
    }

    Json::Value wfs = syu["wfs"];
    //std::cerr << "No wfs: " << wfs.size() << std::endl;

    for (unsigned int i=0; i < wfs.size(); ++i) {
      std::string wf =  wfs[i]["w"].asString();
      if (!wf.length())
	continue;
      unsigned int sno = csdb.find_or_add(wf);
      res.insert(sno);
      //std::cerr << "Wfs " << i << ": " << wf << ", sno " << sno << std::endl;
    }

    return res;
  }
 private:
  StringDB &csdb;
  Json::Reader _reader;
};
*/

#endif
