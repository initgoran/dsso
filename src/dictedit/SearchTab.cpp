#include <iostream>

#include "SearchTab.h"
#include "Textarea.h"
#include "dict.h"

bool SearchTab::set_search_type(std::string string, std::string type) {
 if (!search_last_type.length()) {
    search_more_box->show_all();
    //std::cerr << "Show all!!" << std::endl;
 }
  bool result = (search_last_string == string and search_last_type == type);
  search_last_string = string;
  search_last_type = type;
  if (!string.length())
    return result;
  if (!result) {
    search_word_lbl.set_text(string + ": ");
    //search_no_hits_label.set_text("of ...");
    search_from_entry.set_text("1");
    search_to_entry.set_text("100");
    search_last_from_value = 0;
    search_last_to_value = 0;
  }
    
  return result;
}

SearchTab::SearchTab(MainWindow *master,
		     Glib::RefPtr<Gtk::TextTagTable> tags,
		     std::string word)
  : Tab(master, Gtk::manage(new Textarea(tags, this)), true),
    textarea(dynamic_cast<Textarea *>(main_tab_widget)),
    search_more_box(Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL))),
    the_tag_table(tags)
{
  
  search_from_entry.set_max_length(5);
  search_to_entry.set_max_length(5);
  search_to_lbl.set_text(" to ");
  search_more_box->pack_start(search_word_lbl, Gtk::PACK_SHRINK);
  search_more_box->pack_start(search_from_entry, Gtk::PACK_SHRINK);
  search_more_box->pack_start(search_to_lbl, Gtk::PACK_SHRINK);
  search_more_box->pack_start(search_to_entry, Gtk::PACK_SHRINK);
  //search_more_box->pack_start(search_no_hits_label, Gtk::PACK_SHRINK);
  menuBarBox->pack_start(*search_more_box, Gtk::PACK_SHRINK);
  set_position(250);
  if (word.size()) {
    cb_search(word);
  }
}

SearchTab::~SearchTab()
{
}

void SearchTab::cb_clear() {
  textarea->cb_clear();
  //clear_result();
}

void SearchTab::cb_spell() {
  textarea->cb_spell();
}

unsigned int SearchTab::get_search_value(Glib::ustring val) {
  unsigned int res = 0;
  for (Glib::ustring::iterator i = val.begin(); i != val.end() ; ++i) {
    if (*i > '9' or *i < '0')
      return 0;
    (res *= 10) += (*i-'0');
  }
  if (res)
    --res;
  return res;
}

void SearchTab::set_search_from_value(unsigned int v) {
  search_from_entry.set_text(str_value(v+1));
}

void SearchTab::set_search_to_value(unsigned int v) {
  search_to_entry.set_text(str_value(v+1));
}

unsigned int SearchTab::get_search_from_value() {
  return get_search_value(search_from_entry.get_text());
}

unsigned int SearchTab::get_search_to_value() {
  return get_search_value(search_to_entry.get_text());
}

void SearchTab::cb_search(const Glib::ustring search_str) {
  unsigned int sno = Dict::string_number(search_str.c_str());
  std::string spell;
  if (sno) {
    spell = Dict::spellcheck(sno);
    add_result(search_str.c_str(), Dict::word_count(sno), spell);
  } else {
    spell = Dict::spellcheck(search_str.c_str());
    add_result(search_str.c_str(), 0, spell);
  }
  if (spell == "nok") {
    // Find similar words
    
  }
  set_search_type(search_str, "full word");
  unsigned int from = get_search_from_value();
  unsigned int to = get_search_to_value();
  //std::cerr << "Having " << from << " to " << to << std::endl;
  //std::cerr << "Was " << search_last_from_value << " to " << search_last_to_value << std::endl;
  unsigned int max_hits;
  if (from == search_last_from_value and to == search_last_to_value) {
    if (search_last_to_value) {
      from = search_last_to_value+1;
      to += (search_last_to_value + 1 - search_last_from_value);
    } else 
      to += 100;
  } else if (from == search_last_from_value and to > search_last_to_value) {
    if (search_last_to_value)
      from = search_last_to_value+1;
  } else if (to < from) {
    from = 0;
    to = 99;
  }
  max_hits = to - from + 1;
  //std::cerr << "Having " << from << " and #" << max_hits << std::endl;
  unsigned int tot_hits = textarea->cb_search(search_str, max_hits, from);
  if (tot_hits<max_hits) {
    to = from + tot_hits;
    if (to)
      --to;
  }
  set_search_from_value(from);
  set_search_to_value(to);

  search_last_from_value = from;
  search_last_to_value = to;
  /*
  char *lbl = str_value(tot_hits);
  *--lbl = ' ';
  *--lbl = 'f';
  *--lbl = 'o';
  search_no_hits_label.set_text(lbl);
  */
  //std::cerr << "Showing " << from << " to " << to << std::endl;
}

void SearchTab::cb_search_start(const Glib::ustring search_str) {
  std::string str = search_str;
  unsigned int len = str.size();
  StringSort<FreqCmp>::iterator p = Dict::freq->begin(),
    end = Dict::freq->end();
  unsigned int max_hits = 100;
  while (p != end && max_hits) {
    if (strncmp(str.c_str(), (*Dict::string_db)[*p], len) == 0) {
      add_result((*Dict::string_db)[*p], Dict::word_count(*p), Dict::spellcheck(*p));
      --max_hits;
    }
    ++p;
  }
}


void SearchTab::cb_search_middle(const Glib::ustring search_str) {
  std::string str = search_str;
  StringSort<FreqCmp>::iterator p =
    Dict::freq->begin(), end =
    Dict::freq->end();
  unsigned int max_hits = 100;
  while (p != end && max_hits) {
    if (strstr((*Dict::string_db)[*p], str.c_str())) {
      add_result((*Dict::string_db)[*p], Dict::word_count(*p), Dict::spellcheck(*p));
      --max_hits;
    }
    ++p;
  }
}

void SearchTab::cb_search_end(const Glib::ustring search_str) {
  std::string str = search_str;
  unsigned int len = str.size();
  StringSort<FreqCmp>::iterator p =
    Dict::freq->begin(), end =
    Dict::freq->end();
  unsigned int max_hits = 100;
  while (p != end && max_hits) {
    if (strcmp(str.c_str(), (*Dict::string_db)[*p]+strlen((*Dict::string_db)[*p]) - len) == 0) {
      add_result((*Dict::string_db)[*p], Dict::word_count(*p), Dict::spellcheck(*p));
    }
    ++p;
  }
}

/*
Lexicographic sort:

void SearchTab::cb_search_end(const Glib::ustring search_str) {
  std::string str = search_str;
  unsigned int len = str.size();
  StringSort<RLexiCmp>::iterator p =
    Dict::beta->find_first(str.c_str());
  unsigned int max_hits = 100+1;
  while (p != Dict::beta->end() && --max_hits && strcmp(str.c_str(),
      (*Dict::string_db)[*p]+strlen((*Dict::string_db)[*p]) - len) == 0) {
    //std::cerr << (*Dict::string_db)[*p++] << std::endl;
    add_result((*Dict::string_db)[*p++]);
  }
}

void SearchTab::cb_search_start(const Glib::ustring search_str) {
  std::string str = search_str;
  unsigned int len = str.size();
  StringSort<LexiCmp>::iterator p =
    Dict::alpha->find_first(str.c_str());
  unsigned int max_hits = 100+1;
  while (p != Dict::alpha->end() && --max_hits &&
	 strncmp(str.c_str(), (*Dict::string_db)[*p], len) == 0) {
    //std::cerr << (*Dict::string_db)[*p++] << std::endl;
    add_result((*Dict::string_db)[*p], Dict::word_count(*p));
    ++p;
  }
}
*/

void SearchTab::selected_result(Glib::ustring str, unsigned int score, Glib::ustring info) {
  //std::cerr << "String: " << str << ", score=" << score << "\n";
  //std::cerr << str << ": " << score << std::endl;
  cb_search(str);
}

void SearchTab::new_tab(std::string word, std::string info) {
  add_tab(word, new SearchTab(mw(), the_tag_table, word), word);
}
