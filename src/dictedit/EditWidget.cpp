#include <iostream>

#include <gtkmm/entry.h>
#include <gtkmm/button.h>

#include "dict.h"
#include "Tab.h"
#include "EditWidget.h"

EditWidget::EditWidget(Tab *parent_tab)
  : main_vbox(Gtk::ORIENTATION_VERTICAL),
    edit_grid(0),
    pattern_hbox(Gtk::ORIENTATION_HORIZONTAL),
    tagsLabel("SyU Tags"),
    spellFlagsLabel("Spell Flags"),
    pCompLabel("Compound Prefix"),
    mCompLabel("Compound Middle"),
    bCompLabel("Blocked Prefix"),
    compound_vbox(Gtk::ORIENTATION_HORIZONTAL),
    other_syus_vbox(Gtk::ORIENTATION_VERTICAL),
    corpus_vbox(Gtk::ORIENTATION_VERTICAL),
    suffix_lbl("CHOOSE", Gtk::ALIGN_START),
    prefix_lbl("CHOOSE", Gtk::ALIGN_START),
    next_row_number(0),
    syu(0),
    analyse_syu(0),
    parent(parent_tab)
{
  add(main_vbox);
  main_vbox.pack_start(pattern_hbox, Gtk::PACK_SHRINK);
  pattern_hbox.show();

  suffix_prompt.set_markup("<b>Compound suffix:</b>  ");
  compound_vbox.pack_start(suffix_prompt, Gtk::PACK_SHRINK);
  suffix_prompt.show();
  compound_vbox.pack_start(suffix_box, Gtk::PACK_SHRINK);
  suffix_lbl.show();
  suffix_box.add(suffix_lbl);
  suffix_box.signal_button_press_event().
    connect(sigc::mem_fun(*this, &EditWidget::cb_suffix_menu));
  suffix_box.show();

  {
    tagsBox.add(tagsLabel);
    tagsBox.signal_button_press_event().
      connect(sigc::mem_fun(*this, &EditWidget::cb_tags_menu));
    tagsLabel.show();

    spellFlagsBox.add(spellFlagsLabel);
    spellFlagsBox.signal_button_press_event().
      connect(sigc::mem_fun(*this, &EditWidget::cb_spellFlags_menu));
    spellFlagsLabel.show();

    pCompBox.add(pCompLabel);
    pCompBox.signal_button_press_event().
      connect(sigc::mem_fun(*this, &EditWidget::cb_pcomp_menu));
    pCompLabel.show();

    mCompBox.add(mCompLabel);
    mCompBox.signal_button_press_event().
      connect(sigc::mem_fun(*this, &EditWidget::cb_mcomp_menu));
    mCompLabel.show();

    bCompBox.add(bCompLabel);
    bCompBox.signal_button_press_event().
      connect(sigc::mem_fun(*this, &EditWidget::cb_bcomp_menu));
    bCompLabel.show();
  }

  prefix_prompt.set_markup("       <b>Compound prefix:</b>  ");
  compound_vbox.pack_start(prefix_prompt, Gtk::PACK_SHRINK);
  prefix_prompt.show();
  compound_vbox.pack_start(prefix_box, Gtk::PACK_SHRINK);
  prefix_lbl.show();
  prefix_box.add(prefix_lbl);
  prefix_box.signal_button_press_event().
    connect(sigc::mem_fun(*this, &EditWidget::cb_prefix_menu));
  prefix_box.show();
  
  main_vbox.pack_end(corpus_vbox, Gtk::PACK_SHRINK);
  corpus_vbox.show();
  main_vbox.pack_end(other_syus_vbox, Gtk::PACK_SHRINK);
  other_syus_vbox.show();
  main_vbox.pack_end(compound_vbox, Gtk::PACK_SHRINK);
  compound_vbox.show();
  main_vbox.show();
  check_modified();
  show();
}

EditWidget::~EditWidget() {
}

void EditWidget::cb_remove_widget(Gtk::Widget &wid) {
  wid.hide();
  wid.get_parent()->remove(wid);
}

void EditWidget::cb_clear() {
  //std::cerr << "Edit clear!!" << std::endl;
}

void EditWidget::cb_spell() {
  //std::cerr << "Edit spell!!" << std::endl;
}

void EditWidget::cb_search(const Glib::ustring search_str) {
  //std::cerr << "Edit search for " << search_str << std::endl;

}

  /*
    Kanske en särskild klass för SyuEdit?
   */

void EditWidget::new_syu(std::string word, unsigned int gc, unsigned int tab_id) {
  //std::cerr << "New SyU " << word << ", GC=" << gc << std::endl;
  syu = Dict::dict_db->create_syu(word, gc);
  syu->set_owner(tab_id);
  open_current_syu();
}


void EditWidget::open_syu(unsigned int syuid, unsigned int tab_id) {
  //std::cerr << "Open syu " << syuid << std::endl;
  syu = Dict::dict_db->get_syu(syuid);
  if (syu) {
    if (syu->owner() && syu->owner() != tab_id)
      return;
    syu->set_owner(tab_id);
    open_current_syu();
  }
}

void EditWidget::open_syu(SyU *a_syu, unsigned int tab_id) {
  //std::cerr << "Open new syu..." << std::endl;
  syu = a_syu;
  if (syu) {
    if (syu->owner() && syu->owner() != tab_id)
      return;
    syu->set_owner(tab_id);
    open_current_syu();
  }
}

void EditWidget::open_current_syu() {
  if (!syu)
    return;
  const std::vector<int> *ics;
  std::string gc;
  try {
    if (edit_grid) {
      edit_grid->hide();
      delete edit_grid;
      if (syu)
	syu->revert();
      pattern_hbox.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
      other_syus_vbox.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
      suffixMenu.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
      corpus_vbox.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
      current_other_syuids.clear();
      current_line_numbers.clear();
      current_string_numbers.clear();
    }
    current_other_syuids.insert(syu->id());
    row2wfno.clear();
    next_row_number = 1;
    edit_grid = Gtk::manage(new Gtk::Grid);
    main_vbox.pack_start(*edit_grid);
    gc = syu->gc();
    ics = Dict::dict_db->ics(gc);
  } catch (...) {
    std::cerr << "cannot open SyU" << std::endl;
  }
  try {
    analysis_btn = Gtk::manage(new Gtk::Button("ANALYSE"));
    edit_grid->attach(*analysis_btn, 5, 1, 1, 1);
    analysis_btn->signal_clicked().
      connect( sigc::mem_fun(*this, &EditWidget::cb_analyse) );
    analysis_btn->show();
    
    savebtn = Gtk::manage(new Gtk::Button("SAVE"));
    edit_grid->attach(*savebtn, 5, 0, 1, 1);
    savebtn->signal_clicked().
      connect( sigc::mem_fun(*this, &EditWidget::cb_save_syu) );
    savebtn->show();
    //savebtn->override_background_color(Gdk::RGBA("red"));
    Gtk::Label *lbl1 = Gtk::manage(new Gtk::Label("Inflection type"));
    Gtk::Label *lbl2 = Gtk::manage(new Gtk::Label("Word"));
    Gtk::Label *lbl3 = Gtk::manage(new Gtk::Label("#"));
    Gtk::Label *lbl4 = Gtk::manage(new Gtk::Label("Tags"));
    edit_grid->attach(*lbl1, 0, 0, 1, 1);
    edit_grid->attach(*lbl2, 1, 0, 1, 1);
    edit_grid->attach(*lbl3, 2, 0, 1, 1);
    edit_grid->attach(*lbl4, 3, 0, 1, 1);
    lbl1->show();
    lbl2->show();
    lbl3->show();
    lbl4->show();
  } catch (...) {
    std::cerr << "cannot open SyU 2" << std::endl;
  }

  std::map<unsigned int, unsigned int> unused_ics;

  try {
    // ICs for which no wordform is currently given,
    // mapped to edit_grid y position:

    for (unsigned int i=0; i<ics->size(); ++i) {
      unused_ics[(*ics)[i]] = i+1;
      std::string icname =
	Dict::dict_db->ic((*ics)[i]);
      //std::cerr << "IC: " << icname << std::endl;
      add_edit_grid_row((*ics)[i]);
    }
  } catch (...) {
    std::cerr << "cannot open SyU 3" << std::endl;
  }

  try {
    Gtk::Label *lbl1 = Gtk::manage(new Gtk::Label("Variant wordform"));
    Gtk::Label *lbl2 = Gtk::manage(new Gtk::Label("Word"));
    Gtk::Label *lbl3 = Gtk::manage(new Gtk::Label("#"));
    Gtk::Label *lbl4 = Gtk::manage(new Gtk::Label("Tags"));
    edit_grid->attach(*lbl1, 0, next_row_number, 1, 1);
    edit_grid->attach(*lbl2, 1, next_row_number, 1, 1);
    edit_grid->attach(*lbl3, 2, next_row_number, 1, 1);
    edit_grid->attach(*lbl4, 3, next_row_number, 1, 1);
    lbl1->show();
    lbl2->show();
    lbl3->show();
    lbl4->show();
    ++next_row_number;
  } catch (...) {
    std::cerr << "cannot open SyU 4" << std::endl;
  }

  const std::vector<WF> &wfs = syu->wfs();
  
  // For each existing wordform, store its grid y index
  // in this vector:
  //std::vector<unsigned int> wfpos;

  try {
    for (wfno=0; wfno<wfs.size(); ++wfno) {
      const WF &wf = wfs[wfno];
      unsigned int icno = wf.icno();
      //wfpos.push_back(wfno);
      std::map<unsigned int, unsigned int>::iterator p = unused_ics.find(icno);
      if (p == unused_ics.end()) {
	// Add new
	row2wfno[next_row_number] = wfno;
	add_edit_grid_row(wf.icno(), wf.word(), wf.tagstring());
	//std::cerr << "WF: " << wf.icno() << ", " << wf.word() << ", " << wf.tagstring() << std::endl;
      } else {
	row2wfno[p->second] = wfno;
	Gtk::Entry *entry = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(1, p->second));
	entry->set_text(wf.word());
	Gtk::Entry *entry2 = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(3, p->second));
	entry2->set_text(wf.tagstring());
	unused_ics.erase(p);
      }
    }
    for (std::map<unsigned int, unsigned int>::iterator
	   p = unused_ics.begin(); p != unused_ics.end(); ++p) {
      row2wfno[p->second] = wfno;
      syu->add_wf(wfno, p->first, "");
      ++wfno;
    }
  } catch (...) {
    std::cerr << "cannot open SyU 5" << std::endl;
  }

  // )
  // compound -->  [id1, id2]  id=0 means unknown.

  try {
    //edit_grid->attach(*lbl0, 5, 2, 1, 1);
    edit_grid->attach(tagsBox, 5, 3, 1, 1);
    edit_grid->attach(spellFlagsBox, 5, 5, 1, 1);
    edit_grid->attach(pCompBox, 5, 7, 1, 1);
    edit_grid->attach(mCompBox, 5, 9, 1, 1);
    edit_grid->attach(bCompBox, 5, 11, 1, 1);
    //Gtk::Label *lbl3 = Gtk::manage(new Gtk::Label("Nospell"));
    //edit_grid->attach(*lbl3, 5, 13, 1, 1);
    tagsBox.show();
    spellFlagsBox.show();
    //lbl3->show();
    pCompBox.show();
    mCompBox.show();
    bCompBox.show();
  } catch (...) {
    std::cerr << "cannot open SyU 6" << std::endl;
  }
  try {
    static const char *attr[] = {"tags", "spell_flags", "spell_cp", "spell_cm", "bw"};
    for (unsigned int i=0; i<sizeof(attr)/sizeof(const char *); ++i) {
      Gtk::Entry *entry = Gtk::manage(new Gtk::Entry());
      //entry->set_max_length(24);
      entry->set_text(syu->get_attr(attr[i]));
      edit_grid->attach(*entry, 5, 2*i+4, 1, 1);
      entry->show();
      entry->signal_changed().
	connect(sigc::bind<const char *, Gtk::Entry *>(sigc::mem_fun(*this, &EditWidget::cb_attr_modify), attr[i], entry));}
  } catch (...) {
    std::cerr << "cannot open SyU 7" << std::endl;
  }

  try {
    // Compound prefix + suffix
    unsigned int pf = syu->compound_prefix();
    unsigned int sf = syu->compound_suffix();
    cb_set_prefix(pf);
    cb_set_suffix(sf);
    std::string hw = syu->headword().word();
    //std::cerr << hw << std::endl;
    const std::vector<SyU *> &sufs = Dict::dict_db->possible_suffixes(hw);
    for (unsigned int i=0; i<sufs.size(); ++i) {
      if (sufs[i]->gc() != syu->gc())
	continue;
      Gtk::MenuItem *suffixItem = Gtk::manage(new Gtk::MenuItem(sufs[i]->show_full()));
      suffixItem->signal_activate().
	connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_set_suffix), sufs[i]->id()));
      suffixMenu.add(*suffixItem);
      //std::cerr << hw << ": " << sufs[i]->show_long() << std::endl;
    }

    Gtk::MenuItem *suffixItem;

    suffixItem = Gtk::manage(new Gtk::MenuItem("CHOOSE"));
    suffixItem->signal_activate().
      connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_set_suffix), 0));
    suffixMenu.add(*suffixItem);
      
    suffixItem = Gtk::manage(new Gtk::MenuItem("Not compound"));
    suffixItem->signal_activate().
      connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_set_suffix), SyU::npos));
    suffixMenu.add(*suffixItem);

    suffixMenu.show_all();
  } catch (...) {
    std::cerr << "cannot open SyU 8" << std::endl;
  }
  try {

    WF hw = syu->headword();
    //std::cerr << "HW=" << hw.word() << std::endl;
    if (hw.word().size()) {
      const std::vector<std::string> &pts = Dict::dict_db->patterns(hw.word(), gc);
      //std::cerr << "#PTS: " << pts.size() << std::endl;
      for (unsigned int i=0; i<pts.size(); ++i) {
	   Gtk::Button *btn = Gtk::manage(new Gtk::Button(pts[i]));
	   btn->signal_clicked().connect
	     (sigc::bind<std::string>(sigc::mem_fun(*this, &EditWidget::cb_apply_pattern), pts[i]));
	   btn->show();
	   pattern_hbox.pack_start(*btn, Gtk::PACK_SHRINK);
	   //std::cerr << "Pattern: " << pts[i] << std::endl;
      }
    }
  } catch (...) {
    std::cerr << "cannot open SyU 9" << std::endl;
  }
  edit_grid->show();

}

void EditWidget::cb_set_spellFlags(std::string sflags) {
  if (!syu)
    return;
    //std::cerr << "Modify " << attr;
  Gtk::Entry *entry = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(5, 6));
  entry->set_text(sflags);
  //syu->set_attr("spell_flags", sflags);
  //check_modified();
}

void EditWidget::cb_set_comp(std::string pref, unsigned int row) {
  if (!syu)
    return;
  //std::string::size_type pos = pref.find(" ");
  //if (pos != std::string::npos)
  // pref.resize(pos);
  Gtk::Entry *entry = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(5, row));
  Glib::ustring val = entry->get_text();

  Glib::ustring cval = "," + val + ",";
  Glib::ustring cpref = "," + pref + ",";

  Glib::ustring::size_type i = cval.find(cpref);
  if (i == Glib::ustring::npos) {
    // Add the pref:
    if (val.size())
      val += ",";
    val += pref;
  } else {
    // Remove the pref:
    Glib::ustring::size_type j = cval.find(",", i+1);
    if (j < val.size())
      if (i)
	val = val.substr(0, i) + val.substr(j);	
      else
	val = val.substr(j);
    else
      if (i)
	val = val.substr(0, i-1);
      else
	val = "";
  }
  entry->set_text(val);
}

void EditWidget::cb_set_tags(std::string tag) {
  if (!syu)
    return;
  Gtk::Entry *entry = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(5, 4));
  Glib::ustring val = entry->get_text();
  Glib::ustring cval = "," + val + ",";
  Glib::ustring ctag = "," + tag;
  bool has_value = (tag[tag.length()-1] == '=');
  if (!has_value)
    ctag += ",";
  Glib::ustring::size_type i = cval.find(ctag);
  if (i == Glib::ustring::npos) {
    // Add the tag:
    if (val.size())
      val += ",";
    val += tag;
  } else {
    // Remove the tag:
    Glib::ustring::size_type j = cval.find(",", i+1);
    if (j < val.size())
      if (i)
	val = val.substr(0, i) + val.substr(j);	
      else
	val = val.substr(j);
    else
      if (i)
	val = val.substr(0, i-1);
      else
	val = "";
    /*
    std::cerr << "<" << val.substr(0, i) << "> + <" << cval.substr(j+1) << ">\n";
    } else if (val.size() == i+tag.length()) {
      std::cerr << "<" << val.substr(0, i) << ">\n";
      if (i)
	--i;
      val = val.substr(0, i);
    } else {
      std::cerr << "<" << val.substr(0, i) << "> + <" << val.substr(i+1+tag.length()) << ">\n";
      val = val.substr(0, i) + val.substr(i+1+tag.length());
    }
    */
  }
  //std::cerr << "set tags: " << tag << std::endl;;
  entry->set_text(val);
}

void EditWidget::cb_set_prefix(unsigned int id) {
  syu->set_compound_prefix(id);
  if (id == SyU::npos) {
    prefix_lbl.set_text("Not compound");
  } else if (id) {
    show_other_syu(id);
    SyU *psyu = Dict::dict_db->get_syu(id);
    prefix_lbl.set_text(psyu->show_long());
  } else {
    prefix_lbl.set_text("CHOOSE");
  }
  check_modified();
}
void EditWidget::cb_set_suffix(unsigned int id) {
  syu->set_compound_suffix(id);
  if (id == SyU::npos) {
    suffix_lbl.set_text("Not compound");
  } else if (id) {
    show_other_syu(id);
    SyU *ssyu = Dict::dict_db->get_syu(id);
    suffix_lbl.set_text(ssyu->show_long());
  } else {
    suffix_lbl.set_text("CHOOSE");
  }
  check_modified();
}

static void get_possible_prefs(std::vector<std::string> &prefs) {
  std::string w = prefs[0];
  std::string s = w.substr(0, w.size()-1);
  if (w[w.size()-1] == 'a') {
    prefs.push_back(s);
    prefs.push_back(s+"e");
    prefs.push_back(s+"s");
    prefs.push_back(s+"o");
    prefs.push_back(s+"u");
  } else if (w[w.size()-1] == 'e') {
    prefs.push_back(w+"s");
    prefs.push_back(s);
    prefs.push_back(s+"s");
  } else if (w[w.size()-1] == 'z') {
  } else if (w[w.size()-1] == 'x') {
  } else if (w[w.size()-1] == 's') {
  } else if (w[w.size()-1] == 'm' and w.size()>1 and w[w.size()-2] == 'u') {
    prefs.push_back(w.substr(0, w.size()-2) + "e");
    prefs.push_back(w.substr(0, w.size()-2) + "i");
  } else {
    prefs.push_back(w+"s");    
  }
}

bool EditWidget::cb_pcomp_menu(GdkEventButton* event) {
  if ((event->type != GDK_BUTTON_PRESS) || (event->button != 1 && event->button != 3)) 
    return false;

  if (!syu)
    return false;
  
  std::string word = syu->headword().word();
  if (!word.size())
    return false;
  
  pCompMenu.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
  Gtk::MenuItem *cItem;
  
  std::vector<std::string> prefs;
  prefs.push_back(word);
  get_possible_prefs(prefs);

  for (unsigned int i=0; i<prefs.size(); ++i) {
    std::string word = prefs[i];
    if (prefs[i].length() >= 4) {
      long score =  Dict::huninfodb->signed_prefix_score(word);
      if (score < 0) {
	word += " +";
	word += str_value(-score-1);
      } else {
	word += " ";
	word += str_value(score);
	//std::cerr << "<" << word << ">\n";
      }
    }
    cItem = Gtk::manage(new Gtk::MenuItem(word));
    cItem->signal_activate().
      connect(sigc::bind<std::string, unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_set_comp), prefs[i], 8));
    pCompMenu.add(*cItem);
  }

  if (!pCompMenu.get_attach_widget()) {
    pCompMenu.attach_to_widget(*this);
  }
  pCompMenu.show_all();
  pCompMenu.popup(event->button, event->time);

  return true;
}

bool EditWidget::cb_mcomp_menu(GdkEventButton* event) {
  if ((event->type != GDK_BUTTON_PRESS) || (event->button != 1 && event->button != 3)) 
    return false;

  if (!syu)
    return false;
  
  std::string word = syu->headword().word();
  if (!word.size())
    return false;
  
  mCompMenu.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
  Gtk::MenuItem *cItem;
  
  std::vector<std::string> prefs;
  prefs.push_back(word);
  get_possible_prefs(prefs);

  for (unsigned int i=0; i<prefs.size(); ++i) {
    cItem = Gtk::manage(new Gtk::MenuItem(prefs[i]));
    cItem->signal_activate().
      connect(sigc::bind<std::string, unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_set_comp), prefs[i], 10));
    mCompMenu.add(*cItem);
  }

  if (!mCompMenu.get_attach_widget()) {
    mCompMenu.attach_to_widget(*this);
  }
  mCompMenu.show_all();
  mCompMenu.popup(event->button, event->time);

  return true;
}

bool EditWidget::cb_bcomp_menu(GdkEventButton* event) {
  if ((event->type != GDK_BUTTON_PRESS) || (event->button != 1 && event->button != 3)) 
    return false;

  if (!syu)
    return false;
  
  std::string word = syu->headword().word();
  if (!word.size())
    return false;
  
  bCompMenu.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
  Gtk::MenuItem *cItem;
  
  std::vector<std::string> prefs;
  prefs.push_back(word);
  get_possible_prefs(prefs);

  for (unsigned int i=0; i<prefs.size(); ++i) {
    cItem = Gtk::manage(new Gtk::MenuItem(prefs[i]));
    cItem->signal_activate().
      connect(sigc::bind<std::string, unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_set_comp), prefs[i], 12));
    bCompMenu.add(*cItem);
  }

  if (!bCompMenu.get_attach_widget()) {
    bCompMenu.attach_to_widget(*this);
  }
  bCompMenu.show_all();
  bCompMenu.popup(event->button, event->time);

  return true;
}

bool EditWidget::cb_spellFlags_menu(GdkEventButton* event) {
  //std::cerr << "spellFlags\n";
  if ((event->type != GDK_BUTTON_PRESS) || (event->button != 1 && event->button != 3)) 
    return false;

  spellFlagsMenu.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
  Gtk::MenuItem *sflItem;
  static const char *spflval[] = { "y y y", "y y n", "y n y", "y n n",
				   "b y n", "b n n", "n n y", "n n n" };
  for (unsigned int i=0; i<sizeof(spflval)/sizeof(const char *); ++i) {
    sflItem = Gtk::manage(new Gtk::MenuItem(spflval[i]));
    sflItem->signal_activate().
      connect(sigc::bind<std::string>(sigc::mem_fun(*this, &EditWidget::cb_set_spellFlags), std::string(spflval[i])));
    spellFlagsMenu.add(*sflItem);
  }

  if (!spellFlagsMenu.get_attach_widget()) {
    spellFlagsMenu.attach_to_widget(*this);
  }
  spellFlagsMenu.show_all();
  spellFlagsMenu.popup(event->button, event->time);

  return true;
}

bool EditWidget::cb_tags_menu(GdkEventButton* event) {
  //std::cerr << "Tags\n";
  if ((event->type != GDK_BUTTON_PRESS) || (event->button != 1 && event->button != 3)) 
    return false;

  tagsMenu.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
  Gtk::MenuItem *tItem;
  static const char *tval[] = {
    "informell", "webb", "nocompsug", "obscene",
    "ovanlig", "kontrollera", "sv_FI", "sv_SE",
    "field=", "similar=", "rek="
  };
  for (unsigned int i=0; i<sizeof(tval)/sizeof(const char *); ++i) {
    tItem = Gtk::manage(new Gtk::MenuItem(tval[i]));
    tItem->signal_activate().
      connect(sigc::bind<std::string>(sigc::mem_fun(*this, &EditWidget::cb_set_tags), std::string(tval[i])));
    tagsMenu.add(*tItem);
  }

  if (!tagsMenu.get_attach_widget()) {
    tagsMenu.attach_to_widget(*this);
  }
  tagsMenu.show_all();
  tagsMenu.popup(event->button, event->time);

  return true;
}

bool EditWidget::cb_wf_category_menu(GdkEventButton* event, unsigned int rnum) {
  Gtk::Entry *entry = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(1, rnum));
  if (!entry)
    return false;
  unsigned char curr_cat = 0;
  {
      Glib::ustring wf = entry->get_text();
      unsigned int sno = Dict::string_db->find(wf);
      if (sno)
	curr_cat = Dict::huninfodb->get_spell_cat(sno);
  }
  wfCatMenu.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget));
  for (unsigned int i=0; i<16; ++i) {
    std::string menustr = Dict::huninfodb->spell_category_name[i];
    unsigned char cat = Dict::huninfodb->spell_category_number[i];
    if (cat == curr_cat)
      menustr += " (current)";
    Gtk::MenuItem *item = Gtk::manage(new Gtk::MenuItem(menustr));
    item->signal_activate().
      connect(sigc::bind<unsigned int, unsigned char>(sigc::mem_fun(*this,
	&EditWidget::cb_set_wf_category), rnum, cat));
    wfCatMenu.add(*item);
  }
  wfCatMenu.show_all();
  if (!wfCatMenu.get_attach_widget()) {
    wfCatMenu.attach_to_widget(*this);
  }
  wfCatMenu.popup(event->button, event->time);

  return true;
}

void EditWidget::cb_set_wf_category(unsigned int rnum, unsigned char cat) {
  //std::cerr << "Set: " << rnum << ", " << (int)cat << std::endl;
  Gtk::Entry *entry = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(1, rnum));
  if (!entry)
    return;
  Glib::ustring wf = entry->get_text();
  unsigned int sno = Dict::string_db->find_or_add(wf);
  //std::cerr << "Set: " << wf << "=" << sno << ", cat: " << (int)cat << std::endl;
  Dict::huninfodb->set_spell_cat(sno, cat);
}

bool EditWidget::cb_prefix_menu(GdkEventButton* event) {
  //std::cerr << "cb_prefix_menu" << std::endl;
   if ((event->type == GDK_BUTTON_PRESS) && (event->button == 1 || event->button == 3)) {    
     
     prefixMenu.foreach(sigc::mem_fun(*this, &EditWidget::cb_remove_widget)); 

     unsigned int sf = syu->compound_suffix();
     
     Gtk::MenuItem *prefixItem;

     if (sf) {
       SyU *ssyu = Dict::dict_db->get_syu(sf);
       std::string shw = ssyu->headword().word();
       std::string hw = syu->headword().word();
       if (hw.length() > shw.length()) {
	 size_t plen = hw.length() - shw.length();
	 std::string prefix = hw.substr(0, plen);
	 unsigned int sno = Dict::string_db->find(prefix);
	 if (sno and hw.substr(plen) == shw) {
	   std::set<unsigned int> res = Dict::dict_db->search_wf(sno);
	   if (res.size())
	     for (std::set<unsigned int>::const_iterator p = res.begin();
		  p!=res.end(); ++p) {
	       show_other_syu(*p);
	       SyU *psyu = Dict::dict_db->get_syu(*p);
	       prefixItem = Gtk::manage(new Gtk::MenuItem(psyu->show_full()));
	       prefixItem->signal_activate().
		 connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_set_prefix), psyu->id()));
	       prefixMenu.add(*prefixItem);
	     }
	 }
       }
     }
     prefixItem = Gtk::manage(new Gtk::MenuItem("CHOOSE"));
     prefixItem->signal_activate().
       connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_set_prefix), 0));
     prefixMenu.add(*prefixItem);
      
     prefixItem = Gtk::manage(new Gtk::MenuItem("Not compound"));
     prefixItem->signal_activate().
       connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_set_prefix), SyU::npos));
     prefixMenu.add(*prefixItem);

     prefixMenu.show_all();

     if (!prefixMenu.get_attach_widget()) {
       prefixMenu.attach_to_widget(*this);
     }
     prefixMenu.popup(event->button, event->time);
     return true;
   } else {
     return false;
   }
}
bool EditWidget::cb_suffix_menu(GdkEventButton* event) {
  //std::cerr << "cb_suffix_menu" << std::endl;
   if ((event->type == GDK_BUTTON_PRESS) && (event->button == 1 || event->button == 3)) {    
     if (!suffixMenu.get_attach_widget()) {
       suffixMenu.attach_to_widget(*this);
     }
     suffixMenu.popup(event->button, event->time);
     return true;
   } else {
     return false;
   }
}
void EditWidget::cb_apply_pattern(std::string pname) {
  //std::cerr << "Pattern: " << pname << std::endl;
  Dict::dict_db->apply_pattern(syu, pname);
  refresh_wfs();
}

void EditWidget::check_modified() {
  if (!syu)
    return;
  if (syu->is_modified()) {
    //std::cerr << ": modified" << std::endl;
    savebtn->override_color(Gdk::RGBA("red"));
    //savebtn->show();
  } else {
    //std::cerr << ": not modified" << std::endl;
    savebtn->override_color(Gdk::RGBA("black"));
    //savebtn->hide();
  }
}

void EditWidget::cb_attr_modify(const char *attr, Gtk::Entry *entry) {
  //std::cerr << "Modify " << attr;
  Glib::ustring val = entry->get_text();
  syu->set_attr(attr, val);
  check_modified();
}

void EditWidget::refresh_wfs() {
  for ( std::map<unsigned int,unsigned int>::iterator it = row2wfno.begin();
	it != row2wfno.end(); ++it) try {
      const WF &wfrec = syu->get_wf(it->second);
      std::string w = wfrec.word();
      Gtk::Entry *entry = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(1, it->first));
      //Gtk::Entry *entry2 = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(3, it->first));
      if (!entry /*or !entry2*/) {
	std::cerr << "Cannot find wordform entry at grid row " << it->first << std::endl;
	continue;  // throw ??
      }
      //Glib::ustring wf = entry->get_text();
      //Glib::ustring tag = entry2->get_text();
      //std::cerr << "Have " << wf << ", wants " << w << std::endl;
      entry->set_text(w);
    } catch (std::exception err) {
      std::cerr << "Exception: " << err.what() << ", wordform " << it->second << ", row " << it->first << std::endl;
    }
}

void EditWidget::cb_new_wf(unsigned int icno) {
  for (std::map<unsigned int,unsigned int>::iterator p=row2wfno.begin();
       p != row2wfno.end(); ++p)
    if ((syu->get_wf(p->second)).icno() == icno) {
      Gtk::Entry *entry = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(1, p->first));
      if (entry->get_text().size() == 0)
	// Could use this row, no need to create a new one.
	return;
    }
  
  row2wfno[next_row_number] = wfno;
  syu->add_wf(wfno, icno, "");
  ++wfno;
  add_edit_grid_row(icno);
}
void EditWidget::add_edit_grid_row(unsigned int icno,
				   std::string word,
				   std::string tag) {
  unsigned int rnum = next_row_number++;
  std::string icname =
    Dict::dict_db->ic(icno);

  {
    Gtk::Button *icbtn = Gtk::manage(new Gtk::Button(icname));
    edit_grid->attach(*icbtn, 0, rnum, 1, 1);
    icbtn->show();
    icbtn->signal_clicked().
      connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_new_wf), icno));
  }
  {
    Gtk::Entry *entry = Gtk::manage(new Gtk::Entry());
    //entry->set_max_length(24);
    entry->set_text(word);
    edit_grid->attach(*entry, 1, rnum, 1, 1);
    entry->show();
    entry->signal_activate().
      connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_wf_entry), rnum));
    entry->signal_changed().
      connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_wf_modify), rnum));
  }
  {
    Gtk::Label *lbl = Gtk::manage(new Gtk::Label());
    Gtk::EventBox *box = Gtk::manage(new Gtk::EventBox());

    unsigned int sno = Dict::string_db->find(word);
    unsigned int count = sno ? Dict::word_count(sno) : 0;
    std::string spell = sno ? Dict::spellcheck(sno) : Dict::spellcheck(word.c_str());
    std::string cnt = str_value(count);
    //std::cerr << "DBG: <" << word << "> size=" << word.size() << ", sno=" << sno
    //      << ", count=" << count << ", spell=" << spell << std::endl;
    if (spell == "nok") {
      lbl->set_markup("<span foreground=\"red\">"+cnt+"</span>");
    } else if (spell == "sms") {
      lbl->set_markup("<b>"+cnt+"</b>");
    } else {
      lbl->set_text(cnt);
    }

    box->add(*lbl);
    box->signal_button_press_event().
      connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_wf_category_menu), rnum));
    lbl->show();
    box->show();
    edit_grid->attach(*box, 2, rnum, 1, 1);
  }
  {
    Gtk::Entry *tentry = Gtk::manage(new Gtk::Entry());
    tentry->set_max_length(24);
    tentry->set_text(tag);
    edit_grid->attach(*tentry, 3, rnum, 1, 1);
    tentry->show();
    tentry->signal_activate().
      connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_wf_entry), rnum));
    tentry->signal_changed().
      connect(sigc::bind<unsigned int>(sigc::mem_fun(*this, &EditWidget::cb_wf_modify), rnum));
  }
  {
    Gtk::Label *lbl = Gtk::manage(new Gtk::Label(" "));
    lbl->show();
    edit_grid->attach(*lbl, 4, rnum, 1, 1);
  }
}

void EditWidget::cb_wf_entry(unsigned int row) {
  //std::cerr << "EditWidget::cb_wf_entry() row " << row << std::endl;
}

void EditWidget::cb_wf_modify(unsigned int row) {
  //std::cerr << "EditWidget::cb_wf_modify() row "<< row  << std::endl;
  Gtk::Entry *entry1 = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(1, row));
  Gtk::Entry *entry2 = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(3, row));
  if (!entry1 or !entry2)
    return;  // throw ??
  Glib::ustring wf = entry1->get_text();
  Glib::ustring tag = entry2->get_text();
  /*
  //std::cerr << "Row " << row << " w=" << wf
	//    << ", tags=" << tag << ", wfno=" << row2wfno[row];
  //std::cerr << "SyU: " << syu << std::endl;
  */
  unsigned int sno = Dict::string_db->find(wf);

  Gtk::EventBox *box = dynamic_cast<Gtk::EventBox *>(edit_grid->get_child_at(2, row));
  Gtk::Label *lbl;
  if (box)
    lbl = dynamic_cast<Gtk::Label *>(box->get_child());
  if (lbl) {
    unsigned int count = sno ? Dict::word_count(sno): 0;
    std::string spell = sno ? Dict::spellcheck(sno) : Dict::spellcheck(wf.c_str());
    std::string cnt = str_value(count);
    if (spell == "nok") {
      cnt = "<span foreground=\"red\">"+cnt+"</span>";
    } else if (spell == "sms") {
      cnt = "<b>"+cnt+"</b>";
    }

    if (sno and wf.lowercase() == wf and wf.size() > 1) {
      // OK, count uppercased word too.
      Glib::ustring uword = wf.substr(0, 1).uppercase() + wf.substr(1);
      if (uword != wf) {
	unsigned int sno2 = Dict::string_db->find(uword);
	if (sno2) {
	  unsigned int count2 = Dict::word_count(sno2);
	  if (count2) {
	    cnt += "+";
	    std::string spell2 = Dict::spellcheck(sno2);
	    if (spell2 == "nok") {
	      ((cnt += "<span foreground=\"red\">") += str_value(count2)) += "</span>";
	    } else if (spell2 == "sms") {
	      cnt = "<b>"+cnt+"</b>";
	      ((cnt += "<b>") += str_value(count2)) += "</b>";
	    } else {
	      cnt += str_value(count2);
	    }
	  }
	}
      }
    }
    //std::cerr << "DBG: <" << wf << "> size=" << wf.size() << ", sno=" << sno
    //      << ", count=" << count << ", spell=" << spell << std::endl;
    lbl->set_markup(cnt);
  }

  syu->set_wf(row2wfno[row], wf, tag);
  if (syu->is_modified()) {
    //std::cerr << ": modified" << std::endl;
    savebtn->override_color(Gdk::RGBA("red"));
    //savebtn->show();
  } else {
    //std::cerr << ": not modified" << std::endl;
    savebtn->override_color(Gdk::RGBA("black"));
    //savebtn->hide();
  }
  if (sno and current_string_numbers.find(sno) == current_string_numbers.end()) {

    current_string_numbers.insert(sno);

    const size_t no_examples = 3;
    CPointer cpos[no_examples];
    unsigned int no_hits = Dict::corpus->search(wf, cpos, no_examples);
    for (unsigned int i=0; i < no_hits; ++i) {
      if ( (!i or cpos[i] != cpos[i-1]) and
	   current_line_numbers.find(cpos[i]) == current_line_numbers.end()) {
	current_line_numbers.insert(cpos[i]);
	std::string line = Dict::corpus->line_at(cpos[i]);
	Gtk::Label *lbl = Gtk::manage(new Gtk::Label(line, Gtk::ALIGN_START));
	lbl->show();
	corpus_vbox.pack_start(*lbl, Gtk::PACK_SHRINK);
      }
    }

    std::set<unsigned int> res = Dict::dict_db->search_wf(sno);
    if (res.size())
      for (std::set<unsigned int>::const_reverse_iterator p = res.rbegin();
	   p!=res.rend(); ++p)
	show_other_syu(*p);
  }
}

void EditWidget::show_other_syu(unsigned int syuid) {
  if (current_other_syuids.find(syuid) != current_other_syuids.end())
    return;
  current_other_syuids.insert(syuid);
  SyU *osyu = Dict::dict_db->get_syu(syuid);
  unsigned long score = Dict::tot_score(osyu);
  std::string sc = str_value(score);
  sc += " ";
  sc += osyu->show_full();
  Gtk::Label *lbl = Gtk::manage(new Gtk::Label(sc, Gtk::ALIGN_START));
  lbl->show();
  other_syus_vbox.pack_start(*lbl, Gtk::PACK_SHRINK);
}


void EditWidget::cb_save_syu() {
  if (!syu)
    return;
  //std::cerr << "Save SyU" << std::endl;
  if (syu->is_modified()) {
    try {
      //std::cerr << "Modified" << std::endl;
      unsigned int tab_id = syu->owner();
      syu = Dict::dict_db->save(syu);
      syu->set_owner(tab_id);
      open_current_syu();
    } catch (std::exception ex) {
      std::cerr << "save failed (exception generated) " << ex.what() << std::endl;
    } catch (...) {
      std::cerr << "save failed" << std::endl;
    }
  }
}

void EditWidget::cb_analyse() {
  //std::cerr << "Analyse SyU" << std::endl;
  if (!syu)
    return;
  analyse_syu = syu;
  WCMAP wfs, prefs, nprefs, vprefs;
  syu->get_wfs(wfs);
  Dict::huninfodb->suffix_analysis(wfs, prefs, nprefs, vprefs);
  /*
  for (WCMAP::const_iterator it=wfs.begin(); it != wfs.end(); ++it) {
    std::cout << it->first << ": score=" << it->second << "\n";
  }
  for (WCMAP::const_iterator it=prefs.begin(); it != prefs.end(); ++it) {
    std::cout << it->first << ": prefscore=" << it->second << "\n";
  }
  for (WCMAP::const_iterator it=nprefs.begin(); it != nprefs.end(); ++it) {
    std::cout << it->first << ": nonprefscore=" << it->second << "\n";
  }
  */
  for (std::map<unsigned int,unsigned int>::const_iterator it = row2wfno.begin();
       it != row2wfno.end(); ++it) {
    unsigned int row = it->first;
    //unsigned int wfno = it->second;
    //std::cerr << "EditWidget::cb_analyse() row "<< row << ", wfno=" << wfno << std::endl;
    Gtk::Entry *entry = dynamic_cast<Gtk::Entry *>(edit_grid->get_child_at(1, row));
    if (!entry)
      return;  // throw ??
    Glib::ustring wf = entry->get_text();
    //std::cerr << "Row " << row << " w=" << wf << std::endl;
    WCMAP::const_iterator p=wfs.find(wf);
    if (p != wfs.end() and p->second>0) {
      //std::cerr << "Row " << row << " w=" << wf << " sc=" << p->second << std::endl;
      Gtk::Label *lbl = dynamic_cast<Gtk::Label *>(edit_grid->get_child_at(4, row));
      if (lbl)
	lbl->set_text(str_value(p->second));
    }
  }

  {
    RCMAP sprefs = flip_map(prefs);
    for (RCMAP::const_iterator it=sprefs.begin(); it != sprefs.end(); ++it) {
      //std::cerr << it->second << ": sprefscore=" << it->first << "\n";
      std::string compound = (it->second) + syu->headword().word();
      std::string info = it->second + " (prefix) + ";
      info += str_value(syu->id());
      parent->add_result(compound, it->first, info);
    }
  }

  {
    RCMAP sprefs = flip_map(nprefs);
    for (RCMAP::const_iterator it=sprefs.begin(); it != sprefs.end(); ++it) {
      //std::cerr << it->second << ": nprefscore=" << it->first << "\n";
      std::string compound = (it->second) + syu->headword().word();
      std::string info = it->second + " (non-prefix) + ";
      info += str_value(syu->id());
      parent->add_result(compound, it->first, info);
    }
  }

  {
    RCMAP sprefs = flip_map(vprefs);
    for (RCMAP::const_iterator it=sprefs.begin(); it != sprefs.end(); ++it) {
      //std::cerr << it->second << ": vprefscore=" << it->first << "\n";
      std::string compound = (it->second) + syu->headword().word();
      std::string info = it->second + " (valid prefix) + ";
      info += str_value(syu->id());
      parent->add_result(compound, it->first, info);
    }
  }
}
