#include <iostream>

#include "SpellTab.h"
#include "Textarea.h"
#include "MainWindow.h"
#include "dict.h"

SpellTab::SpellTab(MainWindow *master,
		   Glib::RefPtr<Gtk::TextTagTable> tags)
  : Tab(master, Gtk::manage(new Textarea(tags, this)), true),
    textarea(dynamic_cast<Textarea *>(main_tab_widget)),
    the_tag_table(tags),
    spell_props_lbl("Hunspell:"),
    ok_btn("ok"),
    nok_btn("nok"),
    cpd_btn("sms"),
    spell_cat_lbl(" Cat:"),
    cat_ok_btn("OK"),
    cat_nok_btn("NOK"),
    cat_undecided_btn("???"),
    cat_ignore_btn("---")
{
  set_position(250);
  spell_props_lbl.show();
  ok_btn.set_active();
  nok_btn.set_active();
  cpd_btn.set_active();
  ok_btn.show();
  nok_btn.show();
  cpd_btn.show();
  spell_cat_lbl.show();
  cat_ok_btn.set_active();
  cat_ok_btn.show();
  cat_nok_btn.set_active();
  cat_nok_btn.show();
  cat_undecided_btn.set_active();
  cat_undecided_btn.show();
  cat_ignore_btn.set_active();
  cat_ignore_btn.show();
  menuBarBox->pack_start(spell_props_lbl, Gtk::PACK_SHRINK);
  menuBarBox->pack_start(ok_btn, Gtk::PACK_SHRINK);
  menuBarBox->pack_start(nok_btn, Gtk::PACK_SHRINK);
  menuBarBox->pack_start(cpd_btn, Gtk::PACK_SHRINK);
  menuBarBox->pack_start(spell_cat_lbl, Gtk::PACK_SHRINK);
  menuBarBox->pack_start(cat_ok_btn, Gtk::PACK_SHRINK);
  menuBarBox->pack_start(cat_nok_btn, Gtk::PACK_SHRINK);
  menuBarBox->pack_start(cat_undecided_btn, Gtk::PACK_SHRINK);
  menuBarBox->pack_start(cat_ignore_btn, Gtk::PACK_SHRINK);
}

SpellTab::~SpellTab()
{
}

void SpellTab::cb_clear() {
  textarea->cb_clear();
}

void SpellTab::cb_spell() {
  textarea->cb_spell();
}

void SpellTab::cb_search(const Glib::ustring search_str) {
  textarea->cb_search(search_str);
}

bool SpellTab::show_if(unsigned int sno) {
  std::string spell = Dict::spellcheck(sno);
  //std::cerr << (*Dict::string_db)[sno] << " is: " << spell << std::endl;
  if (spell == "ok") {
    if (!ok_btn.get_active())
      return false;
  } else if (spell == "nok") {
    if (!nok_btn.get_active())
      return false;
  } else if (spell == "sms") {
    if (!cpd_btn.get_active())
      return false;
  }
  std::string cat = Dict::spell_category(sno);
  //std::cerr << "  ... category: " << cat << std::endl;
  if (cat.substr(0, 2) == "OK") {
    if (!cat_ok_btn.get_active())
      return false;
  } else if (cat.substr(0, 3) == "NOK") {
    if (!cat_nok_btn.get_active())
      return false;
  } else if (cat.substr(0, 9) == "UNDECIDED") {
    if (!cat_undecided_btn.get_active())
      return false;
  } else if (cat.substr(0, 6) == "IGNORE") {
    if (!cat_ignore_btn.get_active())
      return false;
  }
  add_result((*Dict::string_db)[sno], Dict::word_count(sno), spell + " / " + cat);
  return true;
}

void SpellTab::cb_search_start(const Glib::ustring search_str) {
  std::string str = search_str;
  unsigned int len = str.size();
  StringSort<FreqCmp>::iterator p = Dict::freq->begin(),
    end = Dict::freq->end();
  unsigned int max_hits = 200;
  for (; p != end && max_hits; ++p) {
    if (strncmp(str.c_str(), (*Dict::string_db)[*p], len) == 0) {
      if (show_if(*p))
	--max_hits;
    }
  }
}

void SpellTab::cb_search_middle(const Glib::ustring search_str) {
  std::string str = search_str;
  StringSort<FreqCmp>::iterator p =
    Dict::freq->begin(), end =
    Dict::freq->end();
  unsigned int max_hits = 200;
  while (p != end && max_hits) {
    if (strstr((*Dict::string_db)[*p], str.c_str())) {
      if (show_if(*p))
	--max_hits;
    }
    ++p;
  }
}

void SpellTab::cb_search_end(const Glib::ustring search_str) {
  std::string str = search_str;
  unsigned int len = str.size();
  StringSort<FreqCmp>::iterator p =
    Dict::freq->begin(), end =
    Dict::freq->end();
  unsigned int max_hits = 100;
  while (p != end && max_hits) {
    if (strcmp(str.c_str(), (*Dict::string_db)[*p]+strlen((*Dict::string_db)[*p]) - len) == 0) {
      if (show_if(*p))
	--max_hits;
    }
    ++p;
  }
}


void SpellTab::selected_result(Glib::ustring str, unsigned int score, Glib::ustring info) {
  //std::cerr << "String: " << str << ", score=" << score << "\n";
  status_show_suggestions(str.c_str());
  mw()->set_search_string(str);
  textarea->cb_textsearch(str);
}

void SpellTab::new_tab(std::string word, std::string info) {
  add_tab(word, new SpellTab(mw(), the_tag_table), word);
}
