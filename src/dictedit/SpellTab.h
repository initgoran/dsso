#ifndef __SPELLTAB_H_
#define __SPELLTAB_H_


#include <gtkmm/texttagtable.h>
#include <gtkmm/togglebutton.h>
#include "Tab.h"
class Textarea;

class SpellTab : public Tab
{

 public:
  SpellTab(MainWindow *master, Glib::RefPtr<Gtk::TextTagTable> tags);
  virtual ~SpellTab();

  virtual void cb_clear();
  virtual void cb_spell();
  virtual void cb_search(const Glib::ustring search_str);
  virtual void cb_search_start(const Glib::ustring search_str);
  virtual void cb_search_middle(const Glib::ustring search_str);
  virtual void cb_search_end(const Glib::ustring search_str);
  virtual void selected_result(Glib::ustring str, unsigned int score, Glib::ustring info);
  virtual void new_tab(std::string word, std::string info = "");
 protected:
 private:
  bool show_if(unsigned int sno);
  Textarea *textarea;
  Glib::RefPtr<Gtk::TextTagTable> the_tag_table;
  Gtk::Label spell_props_lbl;
  Gtk::ToggleButton ok_btn, nok_btn, cpd_btn;
  Gtk::Label spell_cat_lbl;
  Gtk::ToggleButton cat_ok_btn, cat_nok_btn, cat_undecided_btn, cat_ignore_btn;
};

#endif
