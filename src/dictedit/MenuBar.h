#ifndef __MENUBAR_H_
#define __MENUBAR_H_

#include <gtkmm/box.h>
#include <gtkmm/entry.h>
#include <gtkmm/button.h>
#include <gtkmm/image.h>
#include <gtkmm/eventbox.h>

class MainWindow;

class MenuBar : public Gtk::Box
{
 public:
  MenuBar(MainWindow *parent);
  void add_box(Gtk::Box *box);

  void set_search_string(std::string s) {
    search_entry.set_text(s);
  }
  virtual ~MenuBar();

 protected:
  bool cb_main_menu(GdkEventButton* event);
  bool cb_spell_cat_menu(GdkEventButton* event);
  void cb_search();
  void cb_search_start();
  void cb_search_middle();
  void cb_search_end();
  void cb_search_re();
  void cb_search_clear();

  Gtk::Menu main_menu;
  Gtk::Image main_menu_icon;
  Gtk::EventBox main_menu_button;
  Gtk::Menu spell_cat_menu;
  Gtk::Label spell_cat_lbl;
  Gtk::EventBox spell_cat_button;
  Gtk::Button search_button;
  Gtk::Entry search_entry;
  Gtk::Button search_start_button, search_middle_button,
    search_end_button, search_re_button, search_clear_button;
  Gtk::Button quit_button, spell_button, clear_button;
  Gtk::Label _separator;
 private:
  MainWindow *_parent;
};

#endif
