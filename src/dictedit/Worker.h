#ifndef __WORKER_H__
#define __WORKER_H__

#include <gtkmm.h>

class MainWindow;

class Worker
{
public:
  Worker();

  // Thread function.
  void build_hunspell(MainWindow *caller, const char *target,
		      const char *tags);

  void get_info(double *fraction_done, Glib::ustring *msg) const;
  void stop_thread();
  bool stopped() const;

private:
  // Synchronizes access to member data.
  mutable Glib::Threads::Mutex mutex;

  bool shall_stop;
  bool has_stopped;
  double fraction_done;
  Glib::ustring message;
};

#endif
