#include "StringDB.h"

#include <iostream>
#include <fstream>
#include <exception>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include "StringSort.h"

int main(int argc, char *argv[]) {
  
  std::string word = (argc>1) ? argv[1] : "fyra";
  StringDB &sdb = StringDB::get_db();
  unsigned int sno = sdb.find(word);
  std::cerr << word << " has StringNumber: " << sno << std::endl;
  exit(0);
}
