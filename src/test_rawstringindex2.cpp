#include <iostream>
#include "RawStringIndex.h"
#include "SyuGetWordforms.h"

int main(int argc, char *argv[]) {
  if (argc > 1 and strcmp(argv[1], "create") == 0) {
    system("/bin/rm -f test_dssodb.txt test_dssodb.bin test_dssodb.journal test_dssodb_word_index.bin");
    system ("echo -n test_dssodb_word_index.bin 0 0 > test_dssodb_word_index.txt");
  }
  RawStringDB dssodb("test_dssodb.txt", "test_dssodb.bin", "test_dssodb.journal");
  StringDB &csdb = StringDB::get_db();
  RawStringIndex<unsigned int, SyuGetWords> idx(dssodb, "test_dssodb_word_index.txt");

  if (argc > 1 and strcmp(argv[1], "create") == 0) {
    dssodb.set_string(0, "{\"id\":0, \"type\":\"grammar\", \"rev\":1, \"ic\":[\"egennamn\", \"egennamn, genitiv\", \"obestämd form singularis\", \"obestämd form singularis, genitiv\", \"bestämd form singularis\", \"bestämd form singularis, genitiv\", \"obestämd form pluralis\", \"obestämd form pluralis, genitiv\", \"bestämd form pluralis\", \"bestämd form pluralis, genitiv\", \"prefix\", \"interjektion\", \"infinitiv\",\"preteritum\",\"supinum\",\"presens\",\"imperativ\",\"infinitiv, passiv form\",\"preteritum, passiv form\",\"supinum, passiv form\",\"presens, passiv form\",\"perfekt particip, utrum\",\"perfekt particip, neutrum\",\"perfekt particip, plural\",\"presens particip \",\"konjunktiv\",\"positiv utrum\",\"positiv neutrum\",\"bestämd form\",\"pluralis\",\"komparativ\",\"superlativ\",\"superlativ, bestämd form\",\"maskulin\",\"positiv utrum, genitiv\",\"positiv neutrum, genitiv\",\"bestämd form, genitiv\",\"pluralis, genitiv\",\"komparativ, genitiv\",\"superlativ, genitiv\",\"superlativ, bestämd form, genitiv\",\"maskulin, genitiv\",\"adverb\",\"räkneord\",\"förkortning\",\"pronomen\",\"preposition\",\"konjunktion\",\"subjunktion\",\"infinitivmärke\"],\"pattern\":[]}");
    dssodb.set_string(1, "{\"id\":1, \"type\":\"syu\", \"gc\":\"verb\", \"rev\":1, \"wfs\": [{\"ic\":12,\"w\":\"abstrahera\"},{\"ic\":13,\"w\":\"abstraherade\"},{\"ic\":14,\"w\":\"abstraherat\"},{\"ic\":15,\"w\":\"abstraherar\"}]}");
    dssodb.set_string(2, "{\"id\":2, \"spell_cp\":\"ränte,gatu\", \"type\":\"syu\", \"gc\":\"egennamn\", \"rev\":1, \"wfs\":[{\"ic\":0,\"w\":\"Volvo\"},{\"ic\":1,\"w\":\"Volvos\"}]}");
    dssodb.set_string(3, "{\"id\":3, \"type\":\"syu\", \"gc\":\"verb\", \"rev\":1, \"wfs\": [{\"ic\":12,\"w\":\"leta\"},{\"ic\":13,\"w\":\"letade\"},{\"ic\":14,\"w\":\"letat\"},{\"ic\":15,\"w\":\"abstraherar\"}]}");

  } else if (argc > 1 and strcmp(argv[1], "modify") == 0) {
        dssodb.set_string(4, "{\"id\":4, \"type\":\"syu\", \"gc\":\"substantiv\", \"rev\":1, \"wfs\": [{\"ic\":2,\"w\":\"Volvo\"},{\"ic\":3,\"w\":\"Volvos\"},{\"ic\":4,\"w\":\"Volvon\"},{\"ic\":5,\"w\":\"Volvons\"}]}");
	    dssodb.set_string(3, "{\"id\":3, \"type\":\"syu\", \"gc\":\"verb\", \"rev\":1, \"wfs\": [{\"ic\":12,\"w\":\"leta\"},{\"ic\":13,\"w\":\"letade\"},{\"ic\":14,\"w\":\"letat\"},{\"ic\":15,\"w\":\"letar\"}]}");
  } else if (argc > 1) {
    unsigned int sno = csdb.find_or_add(argv[1]);
    const std::set<unsigned int> res = idx.find_ids(sno);
    std::cout << "All IDs with wf " << argv[1] << " (StrNo " << sno << "): ";
    for (std::set<unsigned int>::const_iterator p = res.begin(); p!=res.end(); ++p) {
      std::cout << *p << ' ';
    }
    std::cout << std::endl;
  } else {

  for (RawStringDB::iterator it = dssodb.begin(); it != dssodb.end(); ++it) {
    std::cerr << "String <" << *it << "> at ID " << it.id() << std::endl;
  }

  const char *wfs[8] = {"Volvon", "abstraherat", "Kalle", "Volvos", "abstraherandet",
			"letar", "abstraherar", "Volvo"};

  for (unsigned int i=0; i<sizeof(wfs)/sizeof(const char *); ++i) {
    unsigned int sno = csdb.find_or_add(wfs[i]);
    const std::set<unsigned int> res = idx.find_ids(sno);
    std::cout << "All IDs with wf " << wfs[i] << " (StrNo " << sno << "): ";
    for (std::set<unsigned int>::const_iterator p = res.begin(); p!=res.end(); ++p) {
      std::cout << *p << ' ';
    }
    std::cout << std::endl;
  }
  }
}
