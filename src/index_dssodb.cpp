#include <iostream>
#include <string>
#include "RawStringIndex.h"
#include "SyuGetWordforms.h"

#include "SyU.h"

int main(int argc, char *argv[]) {
  RawStringDB dssodb("dsso_db.txt", "dsso_db.bin", "dsso_db.journal");
  StringDB &csdb = StringDB::get_db();
  RawStringIndex<unsigned int, SyuGetWords> idx(dssodb, "dssodb_word_index.txt");

  if (0) {
    for (RawStringDB::iterator it =dssodb.begin(); it != dssodb.end(); ++it) {
      std::cerr << "String <" << *it << "> at ID " << it.id() << std::endl;
    }
  }

  if (0) {
    const char *wfs1[] = {"", "Volvon", "abstraherat", "Kalle", "Volvos", "abstraherandet",
			  "letar", "abstraherar", "Volvo"};

    const char **wfs = wfs1;
    if (argc > 1) {
      wfs = (const char **)argv;
    } else {
      argc = sizeof(wfs1)/sizeof(const char *);
    }

    for (int i=1; i<argc; ++i) {
      unsigned int sno = csdb.find_or_add(wfs[i]);
      const std::set<unsigned int> res = idx.find_ids(sno);
      std::cout << "All IDs with wf " << wfs[i] << " (StrNo " << sno << "): ";
      for (std::set<unsigned int>::const_iterator p = res.begin(); p!=res.end(); ++p) {
	std::cout << *p << ' ';
      }
      std::cout << std::endl;
    }
  }

  if (0) {
    unsigned int olleid = 71777;
    SyU olle(dssodb.get_string(olleid));
    const std::vector<WF> &wflist = olle.wfs();
    std::cerr << "Orig: " << dssodb.get_string(olleid) << std::endl;
    std::cerr << "Olle: " << olle.gc();
    for (unsigned int i = 0; i < wflist.size(); ++i) {
      std::cerr << " / " << wflist[i].word();
    }
    std::cerr << std::endl;
    olle.set_wf(1, "Olles", "redundant");
    olle.set_wf(0, "OllE", "");
    olle.set_wf(1, "Olles", "");
    std::cerr << "Modified: " << (olle.is_modified() ? "yes" : "no") << std::endl;
    std::cerr << "New:  " << olle.save() << std::endl;
  }

  if (1) {
    unsigned int sno = csdb.find_or_add("hundens");
    const std::set<unsigned int> res = idx.find_ids(sno);
    if (res.size()) {
      unsigned int hundid = *res.begin();
      SyU hund(dssodb.get_string(hundid));
      std::vector<WF> wflist = hund.wfs();
      std::cerr << "Orig: " << dssodb.get_string(hundid) << std::endl;
      std::cerr << "hund: " << hund.gc();
      for (unsigned int i = 0; i < wflist.size(); ++i) {
	std::cerr << " / " << wflist[i].word();
      }
      std::cerr << std::endl;
      //hund.set_wf(1, "Hunds", "redundant");
      //hund.set_wf(0, "Hund", "");
      //hund.set_wf(1, "Hunds", "");
      hund.add_wf(8, 2, "hound", "skoj");
      hund.set_wf(3, "");
      std::cerr << "Modified: " << (hund.is_modified() ? "yes" : "no") << std::endl;
      std::cerr << "New:  " << hund.save() << std::endl;

    }
  }
}
