#include <string.h>
#include <stdlib.h>
#include <iostream>
#include "RawStringDB.h"

int main() {
  RawStringDB strobjdb("dsso_db.txt", "dsso_db.bin", "dsso_db.journal");
  char *str;

  strobjdb.rebuild_index();

  str = strobjdb.get_string(0);
  std::cerr << "Object 0, len=" << strlen(str) << ": " << str << "\n";
  free(str);
  str = strobjdb.get_string(1);
  std::cerr << "Object 1, len=" << strlen(str) << ": " << str << "\n";
  free(str);
  str = strobjdb.get_string(2);
  std::cerr << "Object 2, len=" << strlen(str) << ": " << str << "\n";
  free(str);

}
