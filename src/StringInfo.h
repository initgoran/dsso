#ifndef __STRINGINFO_H__
#define __STRINGINFO_H__

#include "defs.h"
#include "StringDB.h"

class StringInfo {
 public:
 StringInfo(CPointer pos, unsigned int wc = 0, unsigned int d = 0) :
  indexpos(pos),
    word_count(wc),
    dummy(d)
    {
    }
  CPointer indexpos;
  unsigned int word_count;
  unsigned int dummy;
 private:

};

class EmptyStringInfo {
 public:
  StringInfo operator()(StringDB::iterator p) {
    StringInfo info(0);
    return info;
  }
};

#endif
