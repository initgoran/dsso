#include <string.h>
#include <iostream>
#include "RawStringDB.h"

int main() {
  RawStringDB strobjdb("test_str_db.txt", "test_str_db.bin", "test_str_db.journal");
  char *str;

  str = strobjdb.get_string(10);
  std::cout << "Object 10, len=" << strlen(str) << ": " << str << "\n";
  free(str);

  strobjdb.set_string(10, "Id 10 lite efteråt");
  str = strobjdb.get_string(10);
  std::cout << "Object 10, len=" << strlen(str) << ": " << str << "\n";
  free(str);

  str = strobjdb.get_string(7);
  std::cout << "Object 7, len=" << strlen(str) << ": " << str << "\n";
  free(str);

  strobjdb.set_string(4, "Id 4 finns nu");
  strobjdb.set_string(5, "Id 5 finns också");
  strobjdb.set_string(7, "Id 7 är uppdaterad");
  strobjdb.set_string(7, "Id 2 är också uppdaterad");

  str = strobjdb.get_string(2);
  std::cout << "Object 2, len=" << strlen(str) << ": " << str << "\n";
  free(str);
  str = strobjdb.get_string(4);
  std::cout << "Object 4, len=" << strlen(str) << ": " << str << "\n";
  free(str);
  str = strobjdb.get_string(5);
  std::cout << "Object 5, len=" << strlen(str) << ": " << str << "\n";
  free(str);
  str = strobjdb.get_string(7);
  std::cout << "Object 7, len=" << strlen(str) << ": " << str << "\n";
  free(str);

}
