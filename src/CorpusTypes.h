#ifndef __CORPUSTYPES_H__
#define __CORPUSTYPES_H__

#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <string>
#include <map>
#include <exception>
#include "defs.h"

bool has_alphabetic_char(const std::string &s);
bool is_word(const std::string &s);

class BadCorpus : public std::exception {
public:
  virtual const char* what() const throw() {
    return "Bad Corpus";
  }
};

/*
int stringdb_freq_cmp(const void *a, const void *b) {
  unsigned int *aa = (unsigned int *) a;
  unsigned int *bb = (unsigned int *) b;
  const StringInfo &inf_a = ibase_addr[*aa];
  const StringInfo &inf_b = ibase_addr[*bb];
  if (inf_a.word_count > inf_b.word_count)
    return -1;
  if (inf_a.word_count < inf_b.word_count)
    return 1;
  return strcmp(base_addr + *aa * StrLength, base_addr + *bb * StrLength);
}
*/



//const char *const wordlist_filename = "index/wordlist.txt";


// The below stupid global vars are used by qsort cmp functions:

/*
// Points to start of mmapped string DB file:
extern const char *base_addr;

class StringInfo;
// Points to start of mmapped string info file:
extern const StringInfo *ibase_addr;

*/

#endif
