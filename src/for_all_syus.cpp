#include <iostream>
#include <string>

#include "DictDB.h"

int main(int argc, char *argv[]) {
  DictDB dssodb("dsso_db.txt", "dsso_db.bin", "dsso_db.journal", "dssodb_word_index.txt");

  for (DictDB::iterator syu = dssodb.begin(); syu != dssodb.end(); ++syu) {

    std::string nospell = (*syu)->get_attr("nospell");
    if (nospell.size() == 0)
      continue;

    nospell = "," + nospell + ",";
    const std::vector<WF> &wfs = (*syu)->wfs();
    for (unsigned int i=0; i<wfs.size(); ++i) {
      std::string w = "," + wfs[i].word() + ",";
      if (nospell.find(w) == std::string::npos)
	continue;
      (*syu)->add_wf_tag(i, "nospell");
    }
    (*syu)->set_attr("nospell", "");
    dssodb.save(*syu);
    //break;
    /*
    if ((*syu)->not_compound()) {
      std::cout << (*syu)->show_full() << "\n";
      exit(0);
    } else if ((*syu)->compound_prefix() or (*syu)->compound_suffix()) {
      std::cout << (*syu)->headword().word() << ": ";
      unsigned int pf = (*syu)->compound_prefix();
      if (pf) {
	std::cout << dssodb.get_syu(pf)->show_long() << " + ";
      } else {
	std::cout << "??? + ";
      }
      unsigned int sf = (*syu)->compound_suffix();
      if (sf)
	std::cout << dssodb.get_syu(sf)->show_long();
      else
	std::cout << "???";
      std::cout << "\n";
    }
    */
  }
}
