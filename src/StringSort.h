#ifndef __STRINGSORT_H__
#define __STRINGSORT_H__

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <glibmm/ustring.h>

#include "StringDB.h"

template <typename C>
class StringSort {
 public:
  typedef unsigned int size_type;
  typedef int difference_type;
  typedef unsigned int value_type;

  ~StringSort() {
      refresh();
      do_unmap();
      //if (modified)
	//std::cerr << "Saved " << filename << std::endl;
  }

  void refresh() {
    //std::cerr << "refresh " << filename << std::endl;
    add_missing_string_ids();
  }

  unsigned int find(const char *str);

  class iterator {
  public:
    typedef iterator self_type;
    typedef unsigned int value_type;
    typedef unsigned int& reference;
    typedef unsigned int* pointer;
    typedef std::random_access_iterator_tag iterator_category;
    typedef int difference_type;

  iterator(unsigned int *a = 0) : addr(a) {
      //std::cerr << "new iterator, addr: " << addr << std::endl;
    }
    unsigned int &operator*() {
      //std::cerr << "peek " << addr << ", val=" << *addr << std::endl;
      return *addr;
    }
    bool operator==(const iterator &rhs) const {
      return (addr == rhs.addr);
    }
    bool operator!=(const iterator &rhs) const {
      return (addr != rhs.addr);
    }

    iterator &operator++() {
      ++addr;
      return *this;
    }
    iterator operator++(int) {
      iterator it(*this);
      ++addr;
      return it;
    }

    friend bool operator<(const iterator &it1, const iterator &it2) {
      //std::cerr << "cmp " << it1.addr << " and " << it2.addr << std::endl;
      return it1.addr < it2.addr;
    }
    friend bool operator>(const iterator &it1, const iterator &it2) {
      return it1.addr > it2.addr;
    }
    friend bool operator<=(const iterator &it1, const iterator &it2) {
      return it1.addr <= it2.addr;
    }
    friend bool operator>=(const iterator &it1, const iterator &it2) {
      return it1.addr >= it2.addr;
    }
    friend iterator operator-(const iterator &it, size_type n) {
      //std::cerr << "addr / n: " << it.addr << " / " << n << std::endl;
      return iterator(it.addr-n);
    }
    friend difference_type operator-(const iterator &it1, const iterator &it2) {
      return it1.addr-it2.addr;
    }
    friend iterator operator+(const iterator &it, size_type n) {
      return iterator(it.addr+n);
    }
    friend iterator operator+(size_type n, const iterator &it) {
      return iterator(it.addr+n);
    }

    iterator& operator+=(size_type n) {
      addr+=n;
      return *this;
    }

    iterator& operator-=(size_type n) {
      //std::cerr << "addr / n: " << addr << " / " << n << std::endl;
      addr-=n;
      return *this;
    }

    unsigned int operator[](size_type n) const {
      return addr[n];
    }
    iterator& operator--() {
      --addr;
      return *this;
    }
    iterator operator--(int) {
      iterator it(*this);
      --addr;
      return it;
    }
  private:
    unsigned int *addr;
  };
  iterator begin() {
    return iterator(map_addr);
  }
  iterator end() {
    return iterator(map_addr+map_len/sizeof(unsigned int));    
  }
  static StringDB *sdb;

  // Find first string for which cmp(string) is true:
  iterator find_first(const char *str);

  static StringSort *get_db(const std::string &cfg_filename) {
    if (the_db == 0) {
      if (sdb == 0)
	sdb = StringDB::get_dbp();
      the_db = new StringSort(cfg_filename);
    }
    return the_db;
  }

 private:
  StringSort(const StringSort &);
  StringSort(const std::string &cfg_filename);
  static void clean_up() {
    delete the_db;
    the_db = 0;
  }
  static StringSort *the_db;
  void do_mmap();
  void do_unmap();
  bool add_missing_string_ids();
  const std::string config_filename;
  std::string filename;
  unsigned int max_id;
  C cmp;
  unsigned int *map_addr;
  int map_fd;
  size_t map_len;
  bool modified;
};


struct LexiCmp {
  LexiCmp() : sdb(StringDB::get_db()) {
  }
  bool operator()(unsigned int id1, unsigned int id2) {
    return ( strcmp(sdb[id2], sdb[id1]) > 0 );
  }
  int cmp(const char *str1, unsigned int id2) {
    return strcmp(sdb[id2], str1);
  }
  bool filter(const char *str) {
    return true;
  }
private:
  StringDB &sdb;
};

// Compare strings backwards (not optimised):
struct RLexiCmp {
  RLexiCmp() : sdb(StringDB::get_db()) {
  }
  bool operator()(unsigned int id1, unsigned int id2) {
    return _cmp(sdb[id1], sdb[id2]) > 0;
  }
  int cmp(const char *str1, unsigned int id2) {
    return _cmp(str1, sdb[id2]);
  }
  int _cmp(const char *str1, const char *str2) {
    /*
    std::string s1(str1), s2(str2);
    std::string bstr1(s1.rbegin(), s1.rend());
    std::string bstr2(s2.rbegin(), s2.rend());
    return strcmp(bstr2.c_str(), bstr1.c_str());
    */
    while (*str1) ++str1;
    while (*str2) ++str2;
    while (true) {
      --str1;
      --str2;
      if (*str1 != *str2)
	break;
      if (!*str1)
	return 0;      
    }
    if ( (unsigned char) *str2 < (unsigned char) *str1 )
      return -1;
    return 1;
  }
  bool filter(const char *str) {
    if (!*str)
      return false;
    Glib::ustring word(str);
    for (Glib::ustring::iterator i = word.begin(); i != word.end() ; ++i) {
      if (*i > 1023)
	return false;
    }
    return true;
  }
private:
  StringDB &sdb;
};

#include "StringSort.cpp"

#endif
