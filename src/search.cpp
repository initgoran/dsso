#include "StringDB.h"

#include <iostream>
#include <fstream>
#include <exception>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include "StringSort.h"

int main(int argc, char *argv[]) {
  StringDB &sdb = StringDB::get_db();

  StringSort<LexiCmp> *alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");

  const char *word = (argc>1) ? argv[1] : "fyra";
  unsigned int len = strlen(word);
  StringSort<LexiCmp>::iterator p = alpha->find_first(word);
  if (p != alpha->end()) {
    /*
    std::cerr << word << " is before StringNumber: " << sno
	      << " string<" << sdb[*p] << '>' << std::endl;
    */
    while (strncmp(word, sdb[*p], len) == 0) {
      std::cout << sdb[*p++] << std::endl;
    }
	   
  }
  exit(0);
}
