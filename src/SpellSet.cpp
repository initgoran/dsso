#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <fstream>

#include "SpellSet.h"

SpellSet::SpellSet() {

};

void SpellSet::add(SS_TYPE type, std::string word) {
  if (type < No_Types) {
    wset[type].insert(word);
  }
}

bool SpellSet::has(SS_TYPE type, std::string word) {
  if (type < No_Types) {
    return wset[type].count(word);
  }
  return false;
}


SpellSet *SpellSets::dict(std::string cat) {
  std::map<std::string, SpellSet>::iterator it = dicts.find(cat);
  if (it == dicts.end())
    return (SpellSet *) 0;
  return &(it->second);
}

bool SpellSets::has(std::string cat, SpellSet::SS_TYPE type, std::string word) {
  SpellSet *d = dict(cat);
  if (!d) {
    return false;
  }
  return d->has(type, word);
}

void SpellSets::add(std::string cat, SpellSet::SS_TYPE type, std::string word) {
  SpellSet *d = dict(cat);
  if (!d) {
    dicts[cat] = SpellSet();
    d = dict(cat);
  }
  d->add(type, word);
}

const char *SpellSet::filename[No_Types] = {
  "word_cok.txt",
  "word_cnok.txt",
  "word_pre.txt",
  "word_mid.txt",
  "word_suf.txt",
  "word_nok.txt"
};

void SpellSet::save() {
  for (unsigned int i=0; i<No_Types; ++i) {
    std::ofstream out(filename[i]);
    std::set<std::string>::const_iterator it = wset[i].begin();
    while (it != wset[i].end())
      out << *it++ << "\n";
    out.close();
    if (!out) {
      std::cerr << "cannot write file " << filename[i] << std::endl;
      exit(1);
    }
  }
}

void SpellSets::save(std::string dir) {
  if (!dir.length())
    exit(1);

  char curdir[256];
  getcwd(curdir, sizeof(curdir)/sizeof(char));
  
  std::string cmd = "mkdir -p \"" + dir + "\"";
  if (system(cmd.c_str()) != 0) {
    std::cerr << "cannot create directory " << dir << ": " << strerror(errno) << "\n";
    exit(1);
  }

  if ( chdir(dir.c_str()) ) {
    std::cerr << "cannot enter " << dir << std::endl;
    exit(1);
  }
  std::cerr << "Entered directory " << dir << std::endl;
  for (std::map<std::string, SpellSet>::iterator it = dicts.begin();
       it != dicts.end(); ++it) {
    std::string subdir = it->first;
    SpellSet &wset = it->second;
    cmd = "mkdir -p \"" + subdir + "\"";
    std::cerr << "Cmd: " << cmd << ", res=" <<
    system(cmd.c_str())
	      << "\n";
    if ( chdir(subdir.c_str()) ) {
      std::cerr << "cannot enter " << subdir << std::endl;
      exit(1);
    }
    wset.save();
    if ( chdir("..") ) {
      std::cerr << "cannot enter " << subdir << std::endl;
      exit(1);
    }
  }

  chdir(curdir);
  std::cerr << "Entered directory " << curdir << std::endl;
}
