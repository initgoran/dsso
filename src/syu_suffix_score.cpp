#include <iostream>
#include <string>
#include <set>

#include "HunInfoDB.h"
#include "DictDB.h"

int main(int argc, char *argv[]) {
  DictDB dssodb("dsso_db.txt", "dsso_db.bin", "dsso_db.journal", "dssodb_word_index.txt");
  HunInfoDB *hdb = new HunInfoDB();
  std::set<std::string> cok, cnok, nok;
  
  for (DictDB::iterator syu = dssodb.begin(); syu != dssodb.end(); ++syu) {
    if ((*syu)->gc() != "substantiv" and (*syu)->gc() != "adjektiv"
	and (*syu)->gc() != "verb")
      continue;
    
    cok.clear();
    cnok.clear();
    nok.clear();
    (*syu)->all_wfs(cok, cnok, nok);

    if (!cnok.size())
      continue;

    /*
    unsigned long current_score = 0;
    for (std::set<std::string>::const_iterator it = cok.begin();
	 it != cok.end(); ++it) {
      current_score += hdb->valid_suffix_score(it->c_str());
    }
    */
    unsigned long potential_score = 0;
    for (std::set<std::string>::const_iterator it = cnok.begin();
	 it != cnok.end(); ++it) {
      potential_score += hdb->suffix_score(it->c_str());
    }
    std::cout << (*syu)->id() << " " << potential_score << " "
	      << (*syu)->show_long() << std::endl;
    //	      << current_score << std::endl;
  }
}
