#include "DictDB.h"
#include <pcre.h>

// Line is terminated by \n, not ASCII 0.
// Do not treat it as a C string!
// Return ID value, or STRING_OBJECT_NOT_FOUND if not found.
unsigned int get_json_id_val(const char *line) {
  while (true) {
    if (*line == '\n')
      return STRING_OBJECT_NOT_FOUND;
    if (*line == '"' and
	*(line+1) == 'i' and
	*(line+2) == 'd' and
	*(line+3) == '"' and
	*(line+4) == ':') {
      line+=5;
      unsigned int n=0;
      while (*line >= '0' and *line <= '9') {
	n *= 10;
	n += (*line - '0');
	++line;
      }
      return n;
    }
    ++line;
  }
}

const std::vector<SyU *> &DictDB::possible_suffixes(std::string compound) {
  static std::vector<SyU *> syuids;
  syuids.clear();
  for (unsigned int i=1; i+1<compound.size(); ++i) {
    std::string suf = compound.substr(i);
    std::set<unsigned int> sids = search_wf(suf.c_str());
    for (std::set<unsigned int>::iterator it=sids.begin(); it!=sids.end(); ++it) {
      SyU *syu = get_syu(*it);
      if (syu->headword().word() == suf)
	syuids.push_back(syu);
    }
  }
  return syuids;
}

void DictDB::syu_score(WCMAP &wfscore,
		       std::map<unsigned int, unsigned long> &syuscore) {
  for (WCMAP::const_iterator it=wfscore.begin(); it!=wfscore.end(); ++it) {
    std::set<unsigned int> syuids = search_wf((it->first).c_str());
    for (std::set<unsigned int>::const_iterator p=syuids.begin(); p!=syuids.end(); ++p)
      syuscore[*p] += it->second;
  }
}

void DictDB::spell_sets(SpellSets &dicts, std::string exclude_tag_list) {
  std::set<std::string> exclude_tags;
  {
    std::string &s = exclude_tag_list;
    s += ",";
    std::string::size_type pos = 0, next;
    do {
      next = s.find(",", pos);
      exclude_tags.insert(s.substr(pos, next-pos));
      pos = next+1;
    } while (pos < s.length());
  }
  for (iterator syu = begin(); syu != end(); ++syu) {
    (*syu)->wordforms(dicts, exclude_tags);
  }
}

void DictDB::build_hunspell_wordlists(std::string dirname, const char *tags) {
    std::cerr << "build_hunspell_wordlists " << dirname << " tags: " << tags;
    if (dirname.size() < 2)
    return;
  system( ("/bin/rm -fr " + dirname + "/build").c_str() );

  std::cerr << "Writing wordlists... ";
  SpellSets dicts;
  spell_sets(dicts, tags);
  dicts.save( (dirname + "/build").c_str());
}

void DictDB::build_hunspell_make(std::string dirname, const char *target) {
  system( ("make -C " + dirname + " " + target).c_str() );
}

void DictDB::build_hunspell(std::string dirname) {
  build_hunspell_wordlists(dirname, "sv_FI,kontrollera,redundant,ovanlig");
  build_hunspell_make(dirname, "sv_SE.dic");
}

void DictDB::build_fi_hunspell(std::string dirname) {
  build_hunspell_wordlists(dirname, "kontrollera,webb,redundant,ovanlig");
  build_hunspell_make(dirname, "sv_FI.dic");
}

void DictDB::dump_old_format(std::string filename) {
  std::ofstream fd(filename);
  if (!fd) {
    std::cerr << "cannot open " << filename << ": " << strerror(errno) << std::endl;
    return;
  }

  std::string grammar = "{\"meta\":{\"max_id\":1287048,\"server\":\"dsso.se\",\"contact\":\"initgoran@gmail.com\"},\"ordklass\":[\n{\"id\":\"1\",\"rev\":1,\"name\":\"substantiv\",\"ics\":[\"obestämd form singularis\",\"obestämd form singularis, genitiv\",\"bestämd form singularis\",\"bestämd form singularis, genitiv\",\"obestämd form pluralis\",\"obestämd form pluralis, genitiv\",\"bestämd form pluralis\",\"bestämd form pluralis, genitiv\"]},\n{\"id\":\"2\",\"rev\":1,\"name\":\"verb\",\"ics\":[\"infinitiv\",\"preteritum\",\"supinum\",\"presens\",\"imperativ\",\"infinitiv, passiv form\",\"preteritum, passiv form\",\"supinum, passiv form\",\"presens, passiv form\",\"perfekt particip, utrum\",\"perfekt particip, neutrum\",\"perfekt particip, plural\",\"presens particip \",\"konjunktiv\"]},\n{\"id\":\"16\",\"rev\":1,\"name\":\"presens particip\",\"ics\":[\"presens particip\"]},\n{\"id\":\"17\",\"rev\":1,\"name\":\"perfekt particip\",\"ics\":[\"perfekt particip, utrum\",\"perfekt particip, neutrum\",\"perfekt particip, plural\"]},\n{\"id\":\"3\",\"rev\":1,\"name\":\"adjektiv\",\"ics\":[\"positiv utrum\",\"positiv neutrum\",\"bestämd form\",\"pluralis\",\"komparativ\",\"superlativ\",\"superlativ, bestämd form\",\"maskulin\",\"positiv utrum, genitiv\",\"positiv neutrum, genitiv\",\"bestämd form, genitiv\",\"pluralis, genitiv\",\"komparativ, genitiv\",\"superlativ, genitiv\",\"superlativ, bestämd form, genitiv\",\"maskulin, genitiv\"]},\n{\"id\":\"4\",\"rev\":1,\"name\":\"egennamn\",\"ics\":[\"egennamn\",\"egennamn, genitiv\"]},\n{\"id\":\"5\",\"rev\":1,\"name\":\"adverb\",\"ics\":[\"adverb\"]},\n{\"id\":\"6\",\"rev\":1,\"name\":\"räkneord\",\"ics\":[\"räkneord\"]},\n{\"id\":\"7\",\"rev\":1,\"name\":\"deponens\",\"ics\":[\"infinitiv\",\"preteritum\",\"supinum\",\"presens\",\"imperativ\"]},\n{\"id\":\"8\",\"rev\":1,\"name\":\"förkortning\",\"ics\":[\"förkortning\"]},\n{\"id\":\"9\",\"rev\":1,\"name\":\"prefix\",\"ics\":[\"prefix\"]},\n{\"id\":\"10\",\"rev\":1,\"name\":\"pronomen\",\"ics\":[\"pronomen\"]},\n{\"id\":\"11\",\"rev\":1,\"name\":\"interjektion\",\"ics\":[\"interjektion\"]},\n{\"id\":\"12\",\"rev\":1,\"name\":\"preposition\",\"ics\":[\"preposition\"]},\n{\"id\":\"13\",\"rev\":1,\"name\":\"konjunktion\",\"ics\":[\"konjunktion\"]},\n{\"id\":\"14\",\"rev\":1,\"name\":\"subjunktion\",\"ics\":[\"subjunktion\"]},\n{\"id\":\"15\",\"rev\":1,\"name\":\"infinitivmärke\",\"ics\":[\"infinitivmärke\"]}\n],\"syntaktisk_enhet\":[\n";

  fd << grammar;

  iterator syu = begin();
  fd << (*syu)->old_format();
  ++syu;
  for (; syu != end(); ++syu) {
    fd << ",\n" << (*syu)->old_format();
  }
  fd << "\n]}";
}

void DictDB::apply_pattern(SyU *syu, std::string pname) {
  //std::cerr << "Pattern: " << pname << std::endl;
  WF hw = syu->headword();
  if (!hw.word().size())
    return;
  std::string word = hw.word();
  //std::cerr << "Word: " << word << std::endl;
  std::vector<ic_pattern> *slist = _ptn2slist[pname];
  //std::cerr << "Substs: " << slist->size() << std::endl;
  const std::vector<WF> &wfs = syu->wfs();
  for (unsigned int i=0; i<slist->size(); ++i) {
    ic_pattern &icp = (*slist)[i];
    std::string new_word = word;
    //std::cerr << "RE: " << icp.re << ", suffix=" << icp.suffix << std::endl;
    if (icp.re.size()) {
      const char *re_error_string;
      int re_error_offset;
      int re_match_index_vector[10];
      pcre *regexp = pcre_compile(icp.re.c_str(), 0, &re_error_string, &re_error_offset, NULL);
      int rc = pcre_exec(regexp, NULL, word.c_str(), word.size(), 0,
			 0, re_match_index_vector, 10);
      if (rc > 0) {
	//std::cerr << "Match: " << icp.re << " on " << word << " pos " << re_match_index_vector[0] << std::endl;
	new_word.resize(re_match_index_vector[0]);
	new_word += icp.suffix;
      } else {
	//std::cerr << "No match: " << icp.re << " on " << word << std::endl;
      }
    } else
      new_word += icp.suffix;
    //std::cerr << "New word is: " << new_word << std::endl;
    unsigned int wfno = syu->find_wfno(icp.icno);
    if (wfno != SyU::npos) {
      //std::cerr << "Modify word: " << wfno << std::endl;
      syu->set_wf(wfno, new_word);
    } else {
      //std::cerr << "New word: " << syu->next_wfno() << " for icno " << icp.icno << std::endl;
      syu->add_wf(syu->next_wfno(), icp.icno, new_word);
    }
  }
}

const std::vector<std::string> &DictDB::patterns(std::string word, std::string gc) {
  static std::vector<std::string> curr_pat;
  curr_pat.clear();
  std::vector<std::pair<std::string, std::string> > *pts = _gc2plist[gc];
  //std::cerr << "W=" << word << ", GC=" << gc << ", #pts=" << pts->size() << std::endl;
  for (unsigned int i=0; i<pts->size(); ++i) {
    std::string cond = (pts->at(i)).second;
    if (cond.length() > 1) {
      const char *re_error_string;
      int re_error_offset;
      int re_match_index_vector[10];
      pcre *regexp = pcre_compile(cond.c_str(), 0, &re_error_string, &re_error_offset, NULL);
      int rc = pcre_exec(regexp, NULL, word.c_str(), word.size(), 0,
			 0, re_match_index_vector, 10);
      pcre_free(regexp);
      if (rc < 0) {
	//std::cerr << word << " does not match " << cond << ", ptn=" << (pts->at(i)).first << "\n";
	continue;
      }
    }
    //std::cerr << word << " matches " << cond << ", ptn=" << (pts->at(i)).first << "\n";
    curr_pat.push_back((pts->at(i)).first);
  }
  return curr_pat;
}

std::vector<ic_pattern> *DictDB::pattern(std::string pattern_name) {
  if (pattern_cache.find(pattern_name) != pattern_cache.end()) {
    return pattern_cache[pattern_name];
  }

  std::vector<ic_pattern> *pts = new std::vector<ic_pattern>();


  return pts;
}

SyU *DictDB::get_syu(unsigned int id) {
  if (object_cache.find(id) != object_cache.end()) {
    return object_cache[id];
  }

  char *str = _rsdb.get_string(id);
  if (*str == '\0') {
    free(str);
    return 0;
  }
  SyU *syu = new SyU(str);
  free(str);
  object_cache[id] = syu;
  return syu;
}

std::map<unsigned int, SyU *> DictDB::object_cache;
std::map<std::string, std::vector<int> *> DictDB::_gc2ic;
std::map<std::string, unsigned int> DictDB::_gc2gcno;
std::vector<std::string> DictDB::_ics;
std::vector<std::string> DictDB::_gcs;

void DictDB::init_grammar() {
  //std::cerr << "Init grammar:" << std::endl;
  if (_ics.size())
    return;

  SyU *obj = get_syu(0);
  if (!obj) {
    std::cerr << "Bad Grammar" << std::endl;
    exit(1);
  }
  syu_grammar = obj;
  grammar = obj->raw_obj();
  //grammar_classes = grammar["gc"];
  //inflectional_classes = grammar["ic"];
  for (unsigned int i=0; i < grammar["gc"].size(); ++i) {
    std::string gc = grammar["gc"][i]["name"].asString();
    //std::cerr << "GC: " << gc << ", ics=";
    std::vector<int> *v = new std::vector<int>;
    Json::Value iclist = grammar["gc"][i]["ics"];
    for (unsigned int j=0; j < iclist.size(); ++j) {
      unsigned int icno = iclist[j].asInt();
      //std::cerr << " #" << icno;
      v->push_back(icno);
    }
    _gcs.push_back(gc);
    _gc2ic[gc] = v;
    _gc2gcno[gc] = i;
    std::vector<std::pair<std::string, std::string> > *pts = new std::vector<std::pair<std::string, std::string> >();
    _gc2plist[gc] = pts;
    Json::Value plist = grammar["gc"][i]["pattern"];
    for (unsigned int j=0; j < plist.size(); ++j) {
      Json::Value ptn = plist[j];
      std::string name = ptn[0].asString();
      std::string cond = ptn[1].asString() + "$";
      pts->push_back(std::pair<std::string, std::string>(name, cond));
      std::vector<ic_pattern> *slist = new std::vector<ic_pattern>();
      _ptn2slist[name] = slist;
      for (unsigned k=2; k<ptn.size(); ++k) {
	std::string subst = ptn[k].asString();
	if (subst == "0")
	  continue;
	size_t pos = subst.find_first_of('#');
	if (pos == std::string::npos) {
	  slist->push_back( ic_pattern(v->at(k-2), "", subst) );
	} else {
	  slist->push_back( ic_pattern(v->at(k-2), subst.substr(0, pos) + "$",
				       subst.substr(pos+1)) );
	}
      }
      //std::cerr << "Pattern: " << name << " Substs: " << slist->size() <<  std::endl;
    }
    //std::cerr << std::endl;
  }
  for (unsigned int i=0; i < grammar["ic"].size(); ++i) {
    _ics.push_back(grammar["ic"][i].asString());
    //std::cerr << i << ": " << _ics[i] << std::endl;
  }
}
