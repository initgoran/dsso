#include "StringDB.h"

#include <iostream>
#include <fstream>
#include <exception>

#include "StringDB.h"
#include "StringInfo.h"
#include "InfoDB.h"

class Slen {
public:
  unsigned char operator()(StringDB::iterator p) {
    return strlen(*p);
  }
};

int main(int argc, char *argv[]) {
  StringDB &sdb = StringDB::get_db();
  StringDB::iterator p = sdb.begin();

  InfoDB<StringInfo, Slen> &ldb =
    InfoDB<StringInfo, Slen>::get_db("index/stringinfo.bin");
  /*
  unsigned int ldblength = ldb.size();
  std::cerr << "Current length: " << ldblength << std::endl;
  while (p != sdb.end()) {
    std::cerr << "Str: " << *p << " at StringNumber " << p.string_number() << std::endl;
    
    if (p.string_number() > ldblength) {
      ldb.add(p.string_number(), strlen(*p));
      //std::cerr << "Set SN " << p.string_number() << " to " << strlen(*p);
      //std::cerr << ". Current length: " << ldb.size() << std::endl;
    }
    ++p;
  }
  ldblength = ldb.size();
  ldb.update();
  ldb.refresh();
  */
  StringInfo x(1, 1);
  std::cerr << "Size=" << sizeof(x) << std::endl;
  p = sdb.end();
  do {
    --p;
    StringInfo inf = ldb.get_info(p.string_number());    
    std::cerr << "Str: " << *p << "  SN=" << p.string_number() << " count=" << inf.word_count << std::endl;
  } while (p != sdb.begin());

  exit(0);
}
