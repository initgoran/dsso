#include "RawStringDB.h"

#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>

typedef struct { off_t offset; unsigned int objId; } Jrec;

RawStringDB::~RawStringDB() {
  close(index_fd);
  close(db_fd);
  close(journal_fd);
}

void RawStringDB::set_offset(unsigned int id, off_t offset) {
  off_t index_size = index_offset();
  off_t new_offset = index_offset(id);
  if (new_offset < index_size) {
    // Check if we're going to overwrite an existing object:
    if (lseek(index_fd, new_offset, SEEK_SET) != new_offset) {
      throw BadObjDB((("cannot seek in file " + index_filename)+
		      ": ") + strerror(errno));
    }
    off_t res;
    if (read(index_fd, (char *)&res, sizeof(off_t)) != sizeof(off_t)) {
      throw BadObjDB((("cannot read from file " + index_filename)+
		      ": ") + strerror(errno));
    }
    if (lseek(index_fd, new_offset, SEEK_SET) != new_offset) {
      throw BadObjDB((("cannot seek in file " + index_filename)+
		      ": ") + strerror(errno));
    }
    if (res != STRING_OBJECT_NOT_FOUND) {
      // Old object with ID id replaced; write old object's offset to journal:
      Jrec jrec = {res, id};
      if (write(journal_fd, (const char *)&jrec, sizeof(jrec)) != sizeof(jrec)) {
	throw BadObjDB((("cannot write to file " + journal_filename)+
			": ") + strerror(errno));
      }
    }
  } else
    while (new_offset > index_size) {
      if (write(index_fd, (const char *)&STRING_OBJECT_NOT_FOUND, sizeof(STRING_OBJECT_NOT_FOUND)) != sizeof(STRING_OBJECT_NOT_FOUND)) {
	throw BadObjDB((("cannot write to file " + index_filename)+
			": ") + strerror(errno));
      }
      index_size += sizeof(STRING_OBJECT_NOT_FOUND);
    }
  if (write(index_fd, (const char *)&offset, sizeof(off_t)) != sizeof(off_t)) {
    throw BadObjDB((("cannot write to file " + index_filename)+
		    ": ") + strerror(errno));
  }
}

void RawStringDB::get_journal(off_t &journal_pos, unsigned int &id, off_t &offset) {
  if (lseek(journal_fd, journal_pos, SEEK_SET) != journal_pos) {
    throw BadObjDB((("cannot seek in file " + journal_filename)+
		    ": ") + strerror(errno));
  }
  Jrec jrec;
  if (read(journal_fd, (char *)&jrec, sizeof(jrec)) != sizeof(jrec)) {
	throw BadObjDB((("cannot read from file " + journal_filename)+
			": ") + strerror(errno));
  }
  id = jrec.objId;
  offset = jrec.offset;
  journal_pos = lseek(journal_fd, 0, SEEK_CUR);
}

off_t RawStringDB::get_offset(unsigned int id) {
  off_t index_size = index_offset();
  off_t new_offset = index_offset(id);
  if (new_offset >= index_size)
    return STRING_OBJECT_NOT_FOUND;
  if (lseek(index_fd, new_offset, SEEK_SET) != new_offset) {
    throw BadObjDB((("cannot seek in file " + index_filename)+
		    ": ") + strerror(errno));
  }
  off_t res;
  if (read(index_fd, (char *)&res, sizeof(off_t)) != sizeof(off_t)) {
    throw BadObjDB((("cannot read from file " + index_filename)+
		    ": ") + strerror(errno));
  }
  return res;
}

void RawStringDB::rebuild_index() {
  ftruncate(index_fd, 0);
  off_t filelen = lseek(db_fd, 0, SEEK_END);
  if (filelen == 0)
    return;

  char *addr = (char *)
    mmap(NULL, filelen, PROT_READ, MAP_PRIVATE, db_fd, 0);
  if (addr == MAP_FAILED) {
    throw BadObjDB((("cannot mmap file " + db_filename)+
		    ": ") + strerror(errno));
  }
  const char *pos = addr, *end = addr+filelen;
  const char *laststr = addr;

  if (get_id_fp) {
    // get_id_fp maps line to ID number:
    while (pos < end) {
      if (*pos++ == '\n') {
	off_t offset = laststr - addr;
	set_offset((*get_id_fp)(laststr), offset);
	laststr = pos;
      }
    }

  } else {

    // No function to tell us ID from each line;
    // we'll have to assume that line x has ID number x.
    if (lseek(index_fd, 0, SEEK_SET) != 0) {
      throw BadObjDB((("cannot seek in file " + index_filename)+
		      ": ") + strerror(errno));
    }

    while (pos < end) {
      if (*pos++ == '\n') {
	off_t offset = laststr - addr;
	if (write(index_fd, (const char *)&offset, sizeof(off_t)) != sizeof(off_t)) {
	  munmap(addr, filelen);
	  throw BadObjDB((("cannot write to file " + index_filename)+
			  ": ") + strerror(errno));
	}
	laststr = pos;
      }
    }
  }
  munmap(addr, filelen);
}

RawStringDB::RawStringDB(const std::string &db_file, const std::string &index_file,
			 const std::string &journal_file, unsigned int (*fp)(const char *)) :
  db_filename(db_file),
  index_filename(index_file),
  journal_filename(journal_file),
  get_id_fp(fp)
{
  db_fd = open(db_filename.c_str(), O_RDWR | O_CREAT, 0664);
  if (db_fd < 0) {
    throw BadObjDB((("cannot open file " + db_filename)+
		    ": ") + strerror(errno));
  }
  journal_fd = open(journal_filename.c_str(), O_RDWR | O_APPEND | O_CREAT, 0664);
  if (journal_fd < 0) {
    throw BadObjDB(std::string("bad file: ")+journal_filename);
  }
  index_fd = open(index_filename.c_str(), O_RDWR | O_CREAT, 0664);
  if (index_fd < 0) {
    throw BadObjDB((("cannot open file " + index_filename)+
		    ": ") + strerror(errno));
  }
  if (index_offset() == 0)
    rebuild_index();
}

// Append new value to the DB file, update index
void RawStringDB::set_string(unsigned int id, const char *new_value, const char *comment) {
  //std::cerr << "ID=" << id << ", Str=" << new_value << "." << std::endl;
  off_t offset = lseek(db_fd, 0, SEEK_END);
  if (offset < 0) {
    throw BadObjDB((("cannot seek in file " + db_filename)+
		    ": ") + strerror(errno));
  }
  int len = strlen(new_value);
  if (len and (new_value[len-1] == '\n'))
    --len;
  char lf = '\n';
  if (write(db_fd, new_value, len) != len or
      write(db_fd, &lf, 1) != 1) {
    throw BadObjDB((std::string("cannot write to ") + db_filename + ": ") +
		   strerror(errno));
  }
  //std::cerr << "ID=" << id << ", New offset=" << offset << std::endl;
  set_offset(id, offset);
}

char *RawStringDB::get_string(unsigned int id) {
  off_t offset = get_offset(id);
  return get_old_string(offset);
}

char *RawStringDB::get_old_string(off_t offset) {
  if (offset == STRING_OBJECT_NOT_FOUND) {
    // No such object, return empty string:
    char *obj = (char *) malloc(1);
    if (!obj) {
      throw BadObjDB(std::string("cannot malloc: ") + strerror(errno));
    }
    *obj = '\0';
    return obj;
  }
  lseek(db_fd, offset, SEEK_SET);
  size_t obj_size = 256;
  while (true) {
    lseek(db_fd, offset, SEEK_SET);
    char *obj = (char *) malloc(obj_size);
    if (!obj) {
      throw BadObjDB(std::string("cannot malloc: ") + strerror(errno));
    }
    ssize_t rlen = read(db_fd, obj, obj_size);
    if (rlen < 0) {
      free(obj);
      throw BadObjDB((("cannot read from file " + db_filename)+
		      ": ") + strerror(errno));
    }
    for (char *end = obj; end < obj+obj_size; ++end) {
      if (*end == '\n') {
	*end = '\0';
	return obj;
      }
    }
    free(obj);
    if (rlen < (ssize_t) obj_size) {
      // No terminating \n character, bad DB file
      throw BadObjDB(std::string("missing new line in file")+db_filename);
    }
    obj_size *= 2;
  }
}
