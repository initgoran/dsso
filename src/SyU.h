#ifndef __SYU_H__
#define __SYU_H__

#include <limits.h>
#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <set>
#include <vector>

#include <glibmm/ustring.h>
#include <jsoncpp/json/json.h>

#include "defs.h"
#include "SpellSet.h"

class DictObject {
 public:
  virtual ~DictObject() {}

  virtual unsigned int id() const {
    if (obj.isMember("id"))
	return obj["id"].asUInt();
    return 0;
  }

  virtual unsigned int revno() const {
    return obj["rev"].asInt();
  }

  virtual bool is_modified() const = 0;

  virtual std::string save(unsigned int new_id = 0) = 0;

  Json::Value &raw_obj() {
    return obj;
  }

  protected:
  Json::Value obj;
  static Json::Reader _reader;
  static Json::FastWriter _writer;
};

class WF {
 public:
 WF(unsigned int ic = 0, std::string w = "", std::string t = std::string())
   : _icno(ic),
    _wf(w),
    _tags(t)
  {
  }
  void set(std::string w, std::string t = std::string()) {
    _wf = w;
    if (t != _tags) {
      _tags = t;
      _ctags = "," + t + ",";
    }
  }
  bool is_equal(std::string w, std::string t = std::string()) const {
    return (_wf == w and _tags == t);
  }
  bool has_tag(std::string tag) const {
    if (!tag.size() or !_tags.size())
      return false;
    if (!_ctags.size())
      _ctags = ("," + _tags + ",");
    return (_ctags.find("," + tag + ",") != std::string::npos);
  }
  void add_tag(std::string tag) {
    if (!tag.size())
      return;
    if (!_tags.size()) {
      _tags = tag;
      return;
    }
    if (!_ctags.size())
      _ctags = ("," + _tags + ",");
    if (_ctags.find("," + tag + ",") != std::string::npos)
      return;
    (_tags += ",") + tag;
    (_ctags += tag) += ",";
  }
  std::string tagstring() const {
    return _tags;
  }
  const std::string &word() const {
    return _wf;
  }
  unsigned int ulength() const {
    return Glib::ustring(_wf).length();
  }
  const unsigned int icno() const {
    return _icno;
  }
 private:
  unsigned int _icno;
  std::string _wf;
  std::string _tags;
  mutable std::string _ctags;
};

class SyU : public DictObject {
public:
  SyU(const char *str);

  void unset_owner() {
    _owner = 0;
  }

  void set_owner(unsigned int no) {
    _owner = no;
  }

  unsigned int owner() {
    return _owner;
  }

  std::string show_long();
  std::string show_full();

  SyU *get_compound(std::string prefix);

  std::string type() const {
    return obj["type"].asString();
  }

  std::string gc() const {
    if (_gc.size() == 0)
      _gc = obj["gc"].asString();
    return _gc;
  }
  const std::vector<WF> &wfs() const;

  const WF &get_wf(unsigned int wfno);

  void get_wfs(WCMAP &wc) const;

  void wordforms(SpellSets &dict, std::set<std::string> &exclude_tags);
  
  void all_wfs(std::set<std::string> &cok, std::set<std::string> &cnok,
	       std::set<std::string> &nok);
  void cok_wfs(std::set<std::string> &wfset);
  void ok_wfs(std::set<std::string> &wfset);
  //void nok_wfs(std::set<std::string> wfset);
  // Eller beräkna alla i  samma funktion???
  
  void add_wf(unsigned int wfno, unsigned int ic,
	      std::string w, std::string t = std::string());

  // Cannot modify ic, create new instead.
  // Delete by setting w to an empty string.
  void set_wf(unsigned int wfno, std::string w, std::string t = std::string());

  void add_wf_tag(unsigned int wfno, std::string tag);
  
  virtual bool is_modified() const {
    if (!id())
      return true;
    if (c_prefix != c_prefix_orig or c_suffix != c_suffix_orig)
      return true;
    for (std::map<unsigned int, WF>::const_iterator it = modified_wfs.begin();
	 it != modified_wfs.end(); ++it) {
      // Modified existing?
      if (it->first < _wfs.size())
	return true;
      // Added non-empty?
      const WF &wf = it->second;
      const std::string &w = wf.word();
      if (w.size() > 0)
	return true;
    }
    if (modified_attrs.size() > 0)
      return true;
    return false;
  }
  std::string get_attr(const char *attr);
  void set_attr(const char *attr, std::string value);

  virtual std::string save(unsigned int new_id = 0);
  virtual void revert();

  static SyU *create(std::string word, std::string gc, unsigned int ic) {
    std::ostringstream syu_txt;
    syu_txt << "{\"gc\":\"" << gc << "\",\"spell_flags\":\"y n n\",\"wfs\":[{\"w\":\"" << word << "\",\"ic\":" << ic << "}],\"type\":\"syu\",\"rev\":1}";
    return new SyU(syu_txt.str().c_str());
  }

  
  static SyU *create(std::string gc) {
    std::ostringstream syu_txt;
    syu_txt << "{\"gc\":\"" << gc << "\",\"spell_flags\":\"y n n\",\"wfs\":[],\"type\":\"syu\",\"rev\":1}";
    return new SyU(syu_txt.str().c_str());
  }

  std::string old_format();

  WF headword() {
    if (_wfs.size())
      return _wfs[0];
    return WF();
  }

  /* Return value: syuid or
      0   - don't know
     npos - not compound     
  */
  unsigned int compound_prefix() {
    return c_prefix;
  }
  unsigned int compound_suffix() {
    return c_suffix;
  }
  void set_compound_prefix(unsigned int id) {
    c_prefix = id;
  }
  void set_compound_suffix(unsigned int id) {
    c_suffix = id;
  }
  
  bool not_compound() {
    return (c_prefix == npos or c_suffix == npos);
  }
  unsigned int next_wfno() {
    return _next_wfno;
  }
  unsigned int find_wfno(unsigned int icno);
  static const unsigned int npos = UINT_MAX;
 private:
  bool default_compound();
  unsigned int _next_wfno;
  mutable std::string _gc;
  mutable std::vector<WF> _wfs;
  std::map<unsigned int, WF> modified_wfs;
  std::map<const char *, std::string> modified_attrs;
  unsigned int _owner, c_prefix, c_suffix, c_prefix_orig, c_suffix_orig;
};

#endif
