#include <iostream>
#include <string>

#include "DictDB.h"

int main(int argc, char *argv[]) {
  DictDB dssodb("dsso_db.txt", "dsso_db.bin", "dsso_db.journal", "dssodb_word_index.txt");

  SpellSets dicts;

  std::set<std::string> exclude_tags;
  {
    std::string s = (argc == 2) ? argv[1] :
      "sv_FI,kontrollera,redundant,ovanlig";
    s += ",";
    std::string::size_type pos = 0, next;
    do {
      next = s.find(",", pos);
      exclude_tags.insert(s.substr(pos, next-pos));
      pos = next+1;
    } while (pos < s.length());
  }
  for (DictDB::iterator syu = dssodb.begin(); syu != dssodb.end(); ++syu) {
    //if (++n % 1000 == 1)
    //std::cerr << (*syu)->show_long() << std::endl;;
    (*syu)->wordforms(dicts, exclude_tags);
  }
  dicts.save("build");
}
