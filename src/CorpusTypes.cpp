#include "CorpusTypes.h"
#include <glibmm/ustring.h>



// Global copies of addresses to stringsort_file and stringinfo_file
// needed for stand-alone sort functions:
//const char *base_addr = NULL;
//const StringInfo *ibase_addr = NULL;

bool has_alphabetic_char(const std::string &s) {
  Glib::ustring word(s);
  if (!word.validate())
    return false;
  for (Glib::ustring::iterator i = word.begin(); i != word.end() ; ++i)
    if (g_unichar_isalpha(*i))
      return true;
  return false;
}


// Return true if it's a valid utf-8 string that has at least
// one letter, no wide characters, no other characters than
// letters, digits, or the three symbols :.- and not two
// consecutive characters among those three symbols.
bool is_word(const std::string &s) {
  Glib::ustring word(s);
  if (word.bytes() >= MAX_WORD_LENGTH)
    return false;
  if (!word.validate())
    return false;
  bool last_non_alnum = false;
  bool has_letter = false;
  for (Glib::ustring::iterator i = word.begin(); i != word.end() ; ++i) {
    if (*i > 65535) {
      return false;
    } else if (g_unichar_isalpha(*i)) {
      has_letter = true;
      last_non_alnum = false;
    } else if (g_unichar_isdigit(*i)) {
      last_non_alnum = false;
    } else if (*i == ':' or *i == '.' or *i == '-') {
      // Two consecutive .:- means it's not a proper word:
      if (last_non_alnum)
	return false;
      last_non_alnum = true;
    }  else {
      return false;
    }
  }
  return has_letter;
}
