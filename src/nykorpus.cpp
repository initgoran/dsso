/*
Compile with c++  -std=c++11 

Run: 

1. Tokenisering
  - Räkna också antalet rader, eller använd wc!!!!

Kostsamt att radera meningar... lösning???

Ny text:
  - Lagra orden baklängessorterat också, för suffixanalys!!
  - Länka med hunspell!!

Index 0 är reserverat! Börja med en tom rad!

Storlek i index alltid en tvåpotens, minst 4 ???

Datatyp unsigned long !! 

För varje ord, ha två tal:
  - Antal förekomster (8 byte)
  - Startposition i index (8 byte)
    (eller, för bara 1 förekomst, direktpos.)

Index:
  Följd av direktpos, 8 byte styck
  Direktpos 0 = LEDIG!!!!

Control:
  Indexposition (8 byte)


Lediga fyror: Simpel lista av positioner
Lediga åttor:  -"-
osv.

Direktpos: 
  Index för start av _meningen_ (EJ NOLL)

Lediga segment:
       < 1000000 : direkt
       > : lista

Kolla efter dubblettmeningar !!! Efter utländska !!!
   - Minst 50% korrekta ord i meningen!!!
   - om fler än två icke-namn är röda, så "skräpmarkera" den ??

Utökningsbart!!! - Lätt att lägga in nya texter !!!!!!

Server!! Åtkomlig över nätet!!!

*/

#include <iostream>
#include <string>
#include "Corpus.h"




int main(int argc, char *argv[]) {

  /*
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " directory" << std::endl;
    exit(1);
  }
  */
  const int max_hits = 30;

  Corpus corpus;

  CPointer cpos[max_hits];
  if (argc < 2)
    exit(0);
  std::string w1(argv[1]);
  if (true) {
    /*
    std::cout << "Enter two words: ";
    std::cin >> w1 >> w2;
    unsigned int no_hits = corpus.search2(w1, w2, cpos, max_hits);
    std::cout << "Enter word: ";
    std::cin >> w1;
    */
    unsigned int no_hits = corpus.search(w1, cpos, max_hits);
    std::cerr << "Antal: " << no_hits << std::endl;
    for (unsigned int i=0; i < no_hits; ++i) {
      if (!i or cpos[i] != cpos[i-1])
	std::cout << "Pos " << cpos[i] << " <" << corpus.line_at(cpos[i]) << ">" << std::endl;
    }
  }
}
