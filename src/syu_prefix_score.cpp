#include <iostream>
#include <string>
#include <set>

#include "HunInfoDB.h"
#include "DictDB.h"

int main(int argc, char *argv[]) {
  DictDB dssodb("dsso_db.txt", "dsso_db.bin", "dsso_db.journal", "dssodb_word_index.txt");
  HunInfoDB *hdb = new HunInfoDB();
  std::set<std::string> cok;

  WCMAP suffixes, prefixes;
  for (DictDB::iterator syu = dssodb.begin(); syu != dssodb.end(); ++syu) {
    if ((*syu)->gc() != "substantiv" and (*syu)->gc() != "adjektiv"
	and (*syu)->gc() != "verb")
      continue;
    
    cok.clear();
    (*syu)->cok_wfs(cok);

    if (!cok.size()) {
      continue;
    }
    unsigned long potential_score = 0;
    for (std::set<std::string>::const_iterator it = cok.begin();
	 it != cok.end(); ++it) {      
      potential_score += hdb->find_prefixes(*it, suffixes, prefixes);
    }
    std::cout << (*syu)->id() << " " << potential_score << " "
	      << (*syu)->show_long() << std::endl;
  }
  RCMAP scores = flip_map(prefixes);
  for (RCMAP::const_iterator it = scores.begin(); it != scores.end(); ++it)
    std::cerr << it->first << ' ' << it->second << "\n";
}
