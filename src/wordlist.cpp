#include <iostream>
#include <fstream>

#include "StringDB.h"
#include "InfoDB.h"
#include "FreqCmp.h"
#include "StringInfo.h"
#include "StringSort.h"

int main(int argc, char *argv[]) {
  if (argc != 4) {
    std::cerr << "Usage: " << argv[0] << " list_filename freq_filename alpha_filename" << std::endl;
    exit(1);
  }
  std::ofstream listfd(argv[1]);
  if (!listfd) {
    std::cerr << "Cannot open " << argv[1] << ": " << strerror(errno);
    exit(1);
  }
  std::ofstream freqfd(argv[2]);
  if (!freqfd) {
    std::cerr << "Cannot open " << argv[2] << ": " << strerror(errno);
    exit(1);
  }
  std::ofstream lexifd(argv[3]);
  if (!freqfd) {
    std::cerr << "Cannot open " << argv[3] << ": " << strerror(errno);
    exit(1);
  }

  StringDB &sdb = StringDB::get_db();
  
  for (StringDB::iterator it = sdb.begin(); it!= sdb.end(); ++it)
    listfd << *it << std::endl;
  listfd.close();
  if (!listfd)
    std::cerr << "couldn't write " << argv[1] << std::endl;

  {
    StringSort<LexiCmp> *alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");
    StringSort<LexiCmp>::iterator the_end = alpha->end();
    for (StringSort<LexiCmp>::iterator it = alpha->begin(); it != the_end; ++it) {
      //std::cerr << sdb[*it] << ' ' << idb.get_info(*it).word_count << std::endl;
      lexifd << sdb[*it] << std::endl;
    }
  }

  StringSort<FreqCmp> *freq = StringSort<FreqCmp>::get_db("index/freqsort.txt");

  //freq->refresh();

  InfoDB<StringInfo, EmptyStringInfo> &idb = InfoDB<StringInfo, EmptyStringInfo>::get_db("index/stringinfo.bin");
  StringSort<FreqCmp>::iterator the_end = freq->end();
  for (StringSort<FreqCmp>::iterator it = freq->begin(); it != the_end; ++it) {
    //std::cerr << sdb[*it] << ' ' << idb.get_info(*it).word_count << std::endl;
    freqfd << sdb[*it] << ' ' << idb.get_info(*it).word_count << std::endl;
  }
  freqfd.close();
  if (!freqfd)
    std::cerr << "couldn't write " << argv[2] << std::endl;

  exit(0);
}
