#include <iostream>
#include <string>

#include "DictDB.h"

int main(int argc, char *argv[]) {
  if (argc != 6) {
    std::cerr << "Usage: " << argv[0] << " dict word" << std::endl;
  }
  DictDB dict(argv[1], argv[2], argv[3], argv[4]);
  std::vector<SyU *> res = dict.possible_suffixes(argv[5]);
  for (std::vector<SyU *>::const_iterator it = res.begin();
       it!=res.end(); ++it) {
    SyU *syu = *it;
    std::cout << syu->show_long() << std::endl;
  }
}
