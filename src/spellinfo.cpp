#include "string.h"

#include "StringDB.h"
#include "StringInfo.h"
#include "StringSort.h"
#include "FreqCmp.h"
#include "HunInfoDB.h"

int main(int argc, char *argv[]) {
  StringDB &sdb = StringDB::get_db();
  HunInfoDB hdb;
  InfoDB<StringInfo, EmptyStringInfo> &infodb = InfoDB<StringInfo, EmptyStringInfo>::get_db("index/stringinfo.bin");
  hdb.refresh();

  if (argc > 1) {
    if (strcmp(argv[1], "reload_base") == 0) {
      hdb.reload_dict(TheDicts[1], sdb.begin(), sdb.end());
    } else if (strcmp(argv[1], "reload_devel") == 0) {
      hdb.reload_dict(TheDicts[0], sdb.begin(), sdb.end(), true, 1);
    } else if (strcmp(argv[1], "reload_sv_se") == 0) {
      hdb.reload_dict(TheDicts[2], sdb.begin(), sdb.end());
    } else if (strcmp(argv[1], "reload_sv_fi") == 0) {
      hdb.reload_dict(TheDicts[3], sdb.begin(), sdb.end());
      //} else if (strcmp(argv[1], "find_prefixes") == 0) {
      //hdb.find_prefixes(argv);
    } else {


      std::vector<unsigned int> strings;
      if (argc == 3 and (strcmp(argv[1], "start_ok") == 0 or
			 strcmp(argv[1], "start_nok") == 0)) {
	hdb.starts_with(argv[2], strcmp(argv[1], "start_ok") == 0, strings);
	for (unsigned int i=0; i<strings.size(); ++i)
	  std::cerr << sdb[strings[i]] << ' ' << infodb.get_info(strings[i]).word_count
		    << std::endl;
      } 

      if (argc == 3 and (strcmp(argv[1], "end_ok") == 0 or
			 strcmp(argv[1], "end_nok") == 0)) {
	hdb.ends_with(argv[2], strcmp(argv[1], "end_ok") == 0, strings);
	for (unsigned int i=0; i<strings.size(); ++i)
	  std::cerr << sdb[strings[i]] << ' ' << infodb.get_info(strings[i]).word_count
		    << std::endl;
      } 

    }
  } else {
    std::ofstream okfh("afreq_finns.txt"), cpdfh("afreq_sms.txt"), nokfh("afreq_saknas.txt");
    StringSort<FreqCmp> *freq = StringSort<FreqCmp>::get_db("index/freqsort.txt");
    freq->refresh();
    InfoDB<StringInfo, EmptyStringInfo> &idb = InfoDB<StringInfo, EmptyStringInfo>::get_db("index/stringinfo.bin");
    StringSort<FreqCmp>::iterator the_end = freq->end();
    for (StringSort<FreqCmp>::iterator it = freq->begin(); it != the_end; ++it) {
      unsigned char stat = hdb.spell_status(*it); 
      std::ofstream &fh = (stat & SPELL_IS_COMPOUND) ? cpdfh : ((stat & SPELL_DEVEL_OK) ? okfh : nokfh);
      fh << sdb[*it] << ' ' << idb.get_info(*it).word_count << std::endl;
    }
  }
  exit(0);
}
