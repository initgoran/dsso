#ifndef __DEFS_H__
#define __DEFS_H__

#include <sys/types.h>
#include <exception>
#include <string>
#include <map>
#include <algorithm>

class StringNumber {
 public:
 StringNumber(unsigned int n) : _no(n) {}
  unsigned int no() {
    return _no;
  }
 private:
  unsigned int _no;
};

class BadIndex : public std::exception {
 public:
 BadIndex(std::string message)
   : msg(message) {
  }
  virtual ~BadIndex() throw() {
  }
  virtual const char* what() const throw() {
    return msg.c_str();
  }
 private:
  std::string msg;
};


typedef std::map<std::string, unsigned int> WCMAP;
typedef std::multimap<unsigned int, std::string, std::greater<unsigned int> > RCMAP;

// Used for positions within a huge file:
typedef off_t CPointer;

const unsigned int StrLength = 8;

// The number of bytes in a word's utf-8 encoding
// must be less than this, otherwise it will be ignored:
const unsigned int MAX_WORD_LENGTH = 48;

template<typename A, typename B>
std::pair<B,A> flip_pair(const std::pair<A,B> &p)
{
    return std::pair<B,A>(p.second, p.first);
}

template<typename A, typename B>
std::multimap<B, A, std::greater<B> > flip_map(const std::map<A,B> &src)
{
    std::multimap<B, A, std::greater<B> > dst;
    std::transform(src.begin(), src.end(), std::inserter(dst, dst.begin()), 
                   flip_pair<A,B>);
    return dst;
}

#endif
