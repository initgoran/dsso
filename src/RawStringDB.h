#ifndef __RAWSTRINGDB_H__
#define __RAWSTRINGDB_H__

#include <unistd.h>
#include <sys/types.h>
#include <string>

class BadObjDB : public std::exception {
public:
  BadObjDB(std::string message = "Bad Object DB")
    : msg(message) {
  }
  virtual ~BadObjDB() throw() {
  }
  virtual const char* what() const throw() {
    return msg.c_str();
  }
 private:
  std::string msg;
};

const off_t STRING_OBJECT_NOT_FOUND = (off_t) -1;

// Journal file format: List of binary (off_t) offsets into the
// db_filename, one for each string that has been deleted or replaced
// by a new version
class RawStringDB {
public:
  RawStringDB(const std::string &db_file, const std::string &index_file, const std::string &journal_file, unsigned int (*fp)(const char *) = 0);
  ~RawStringDB();

  void set_string(unsigned int id, const char *new_value, const char *comment = NULL);
  char *get_string(unsigned int id);
  char *get_old_string(off_t offset);

  // Assuming the database contains string ID n at line n for each n,
  // rebuild the index file:
  void rebuild_index();

  off_t db_offset() {
    return lseek(db_fd, 0, SEEK_END);
  }
  unsigned int next_id() {
    return ( index_offset()/sizeof(off_t) );
  }
  off_t index_offset() {
    return lseek(index_fd, 0, SEEK_END);
  }
  void get_journal(off_t &journal_pos, unsigned int &id, off_t &offset);
  off_t journal_offset() {
    return lseek(journal_fd, 0, SEEK_END);
  }
  bool object_exists(unsigned int id) {
    return get_offset(id) != STRING_OBJECT_NOT_FOUND;
  }
  class iterator {
  public:
  iterator(unsigned int id = 0, RawStringDB *rsdb = 0) : theDB(rsdb), _id(id) {
    }
    unsigned int id() { return _id; }
    iterator& operator=(const iterator &rhs) {
      theDB = rhs.theDB;
      _id = rhs._id;
      return *this;
    }
    bool operator==(const iterator &rhs) const {
      return (_id == rhs._id && theDB == rhs.theDB);
    }
    bool operator!=(const iterator &rhs) const {
      return (_id != rhs._id || theDB != rhs.theDB);
    }

    iterator &operator++() {
      unsigned int max = theDB->next_id();
      if (_id >= max)
	return *this;
      do {
	++_id;
      } while (_id < max and !theDB->object_exists(_id));
      return *this;
    }

    iterator operator++(int) {
      iterator it(*this);
      ++(*this);
      return it;
    }

    iterator &operator--() {
      if (_id == 0)
	return *this;
      do {
	--_id;
      } while (_id > 0 and !theDB->object_exists(_id));
      return *this;
    }

    iterator operator--(int) {
      iterator it(*this);
      --(*this);
      return it;
    }
    const char *operator*() const {
      return theDB->get_string(_id);
    }
  private:
    RawStringDB *theDB;
    unsigned int _id;
  };
  iterator begin() {
    return iterator(0, this);
  }
  iterator end() {
    return iterator(next_id(), this);
  }
private:
  off_t get_offset(unsigned int id);
  void set_offset(unsigned int id, off_t offset);
  off_t index_offset(unsigned int id) {
    return (off_t)id * sizeof(off_t);
  }
  std::string db_filename, index_filename, journal_filename;
  int index_fd, db_fd;
  int journal_fd;
  unsigned int (*get_id_fp)(const char *);
};

#endif
