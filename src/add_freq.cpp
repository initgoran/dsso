#include <string.h>
#include <fstream>
#include <set>

#include "StringDB.h"
#include "StringInfo.h"
#include "FreqCmp.h"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "usage: " << argv[0] << " filename" << std::endl;
    exit(1);
  }
  StringDB &sdb = StringDB::get_db();
  InfoDB<StringInfo, EmptyStringInfo> &infodb = InfoDB<StringInfo, EmptyStringInfo>::get_db("index/stringinfo.bin");
  std::ifstream fd(argv[1]);
  if (!fd) {
    std::cerr << "cannot open " << argv[1] << ": " << strerror(errno) << std::endl;
    exit(1);
  }
  while (fd) {
    std::string line;
    getline(fd, line);
    std::istringstream lfd(line);
    std::ostringstream res;
    std::string word;
    unsigned long score = 0;
    std::set<unsigned int> s;
    while (lfd >> word) {
      unsigned int sno = sdb.find(word);
      if (!sno) {	
	continue;
      }
      if (s.find(sno) != s.end())
	continue;
      s.insert(sno);

      unsigned long c = infodb.get_info(sno).word_count;
      std::cout << word << ' ' << c << '\n';
    }
  }
}
