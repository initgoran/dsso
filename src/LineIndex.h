#ifndef __LINEINDEX_H__
#define __LINEINDEX_H__

#include <string>
#include <set>

class LineIndexFile {
public:
  LineIndexFile(int the_fd, std::string the_filename);
  bool find_key(size_t key);
  off_t size() const {
    return _max_pos;
  }
  std::string name() const {
    return filename;
  }
  int fd_no() const {
    return fd;
  }
private:
  size_t read_pos(off_t pos);
  void read_len(void *buf, off_t pos, size_t no_words);
  int fd;
  std::string filename;
  off_t _max_pos;
  size_t _min_val, _max_val;
};

struct SmallerFile {
  bool operator()(LineIndexFile *lhs, LineIndexFile *rhs) {
    return (lhs->size() < rhs->size() or lhs->name() < rhs->name());
  }
};

class LineIndex {
public:
  LineIndex(std::string dirname);
  ~LineIndex();
  bool add_line(std::string line);
private:
  unsigned int max_file_no;
  std::string index_dirname;
  void clear_cache(bool load_new_file = true);
  std::set<size_t> cache;
  typedef std::set<LineIndexFile *, SmallerFile> FileSet;
  FileSet ifile;
};


/*

#include <vector>
#include <map>
#include <fstream>

class BadScanner : public std::exception {
public:
  BadScanner(std::string explain) : err(explain) {
  }
  virtual const char* what() const throw() {
    return err.c_str();
  }
private:
  std::string err;
};


class CorpusScanner {
public:
  CorpusScanner(const std::string filename, unsigned int no_lines);
  CorpusScanner();
  void scan();
  void merge();
  void rescan(const char *filename_txt);
  void open_all_files(const char *dirname_val, const char *output_file = 0);
private:
  size_t *value;
  std::ifstream corpus_fd;
  unsigned int line_count;
  unsigned int count = 0;
  std::map<size_t, bool> stash;
  std::vector<Hbuf *> vfile;
  int output_fd;
};
*/

#endif
