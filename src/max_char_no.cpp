#include <iostream>
#include <fstream>
#include <string>

#include <glibmm/ustring.h>


int main(int argc, char *argv[]) {
  if (argc == 2) {
    std::ifstream corpus_fd(argv[1]);
    std::string line;
    unsigned int max = 0;
    while ( getline(corpus_fd, line)) {
      Glib::ustring word(line);
      int pos = -1, mp = -1;
      for (Glib::ustring::iterator i = word.begin(); i != word.end() ; ++i) {
	++pos;
	if (*i > max) {
	  max = *i;
	  mp = pos;
	}
      }
      if (mp >= 0)
	std::cerr << line << ": pos " << mp << " <" << max << ">" << std::endl;
    }
    std::cerr << "Max = " << max << std::endl;
  } else {
    std::cerr << "Usage: " << argv[0] << " infile" << std::endl;
    exit(1);
  }
  exit(0);
}
