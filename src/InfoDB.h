#ifndef __INFODB_H__
#define __INFODB_H__

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <string.h>

#include <iostream>
#include <fstream>
#include "StringDB.h"
#include "defs.h"

/*
   At position (n) in this file, we have information about the string with
   StringNumber (n). By "position" we mean data of size (s) at offset (s)*(n),
   where (s) is the number of bytes in an object of type C,
   the template parameter.
*/

template <class C, class F>
class InfoDB {
 public:
  static InfoDB<C,F> &get_db(const char *filename) {
    return *get_dbp(filename);
  }
  static InfoDB<C,F> *get_dbp(const char *filename) {
    if (the_db == 0)
      the_db = new InfoDB(filename);
    // Check it's the same filename!!
    //if (filename != stringinfo_filename)
    //throw BadIndex();
    return the_db;
  }

  ~InfoDB();

  // Add a new record at string_number, which must be at the end of the DB file.
  // Use this to avoid auto-updating of the info file.
  void add(unsigned int string_number, const C &inf) {
    if (curr_map_size < string_number) {
      remap_infofile();
      if (curr_map_size < string_number)
	throw BadIndex("Illegal string number");
    }
    memcpy( (void *)(stringinfo_addr+string_number), (const void *)&inf, sizeof inf);
    if (curr_largest_position < string_number)
      curr_largest_position = string_number;
  }

  void set_info(unsigned int wpos, const C &inf) {
    (*this)[wpos] = inf;
  }

  C get_info(unsigned int wpos) {
    return (*this)[wpos];
  }

  unsigned int word_count(unsigned int wpos) {
    return (*this)[wpos].word_count;
  }

  C &operator[](unsigned int wpos) {
    if (curr_largest_position < wpos) {
      update();
      if (wpos >= curr_map_size) {
	std::cerr << "ERROR (fatal): illegal string number " << wpos << std::endl;
	return stringinfo_addr[0];
      }
    }
    return stringinfo_addr[wpos];
  }

  void update();

 private:

  InfoDB(const char *filename);

  void remap_infofile();

  static void clean_up() {
    if (the_db) {
      delete the_db;
      the_db = 0;
    }
  }
  static InfoDB *the_db;
  int stringinfo_fd;

  // Position of memory mapping:
  C *stringinfo_addr;

  // Mapped part of DB file:
  size_t stringinfo_maplen;

  // Number of info positions within mapped memory.
  unsigned int curr_map_size;

  // Largest assigned position:
  unsigned int curr_largest_position;

  F func;
  const char *stringinfo_filename;
  StringDB &sdb;
};

template <class C, class F>
InfoDB<C,F> *InfoDB<C,F>::the_db = 0;

#include "InfoDB.cpp"

#endif
