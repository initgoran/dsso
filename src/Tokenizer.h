#ifndef __TOKENIZER_H__
#define __TOKENIZER_H__

#include <string>
#include <deque>
#include <fstream>

#include <glibmm/ustring.h>

class HunInfoDB;

class Tokenizer {
 public:
  Tokenizer(HunInfoDB *hinfo);
  void tokenize_line(const Glib::ustring &line, std::deque<int> &word_pos,
		     unsigned int &possible_sentence_end);
  bool start_file(std::string filename, bool pre_tokenized = false);

  // Will return next sentence from the file last given by the above function.
  // Each nonempty substring followed by a space is a word token. Nonempty substrings
  // followed by a tab are not word tokens.
  // On end of file, an empty string will be returned.
  std::string next_line();

 private:
  unsigned int singledot(Glib::ustring::const_iterator beg,
			 Glib::ustring::const_iterator end,
			 std::deque<int> &wordindex, unsigned int offset);
  unsigned int multidots(Glib::ustring::const_iterator beg, Glib::ustring::const_iterator end,
			 std::deque<int> &wordindex, unsigned int offset);
  unsigned int check_string(Glib::ustring::const_iterator beg, Glib::ustring::const_iterator end,
			    std::deque<int> &wordindex, unsigned int offset);
  HunInfoDB *huninfodb;
  std::ifstream corpus_fd;
  bool file_is_pre_tokenized;
  std::deque<std::string> curr_lines;
  std::string curr_line;
  unsigned int possible_sentence_end;
};

#endif
