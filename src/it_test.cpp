#include "StringDB.h"

#include <iostream>
#include <fstream>
#include <exception>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include "StringSort.h"

int main(int argc, char *argv[]) {
  StringDB &sdb = StringDB::get_db();
  
  /*
  StringDB::iterator p = sdb.begin();
  std::cerr << "Pos 1: " << sdb[1] << std::endl;
  while (p != sdb.end()) {
    std::cerr << "Str: " << *p << " at StringNumber " << p.string_number() << std::endl;
    ++p;
  }

  p = sdb.end();
  do {
    --p;
    std::cerr << "Str: " << *p << std::endl;
  } while (p != sdb.begin());
  */

  StringSort<LexiCmp> *alpha = StringSort<LexiCmp>::get_db("index/lexisort.txt");
  StringSort<RLexiCmp> *beta = StringSort<RLexiCmp>::get_db("index/rlexisort.txt");

  alpha->refresh();
  StringSort<LexiCmp>::iterator the_end = alpha->end();
  for (StringSort<LexiCmp>::iterator it = alpha->begin(); it != the_end; ++it) {
    std::cerr << "Sort str: " << sdb[*it] << std::endl;
  }

  beta->refresh();
  std::cerr << "Reverse lexicographic:" << std::endl;
  for (StringSort<RLexiCmp>::iterator it = beta->begin(); it != beta->end(); ++it) {
    std::cerr << "Sort str: " << sdb[*it] << std::endl;
  }

  if (argc > 1) {
    unsigned int sno = alpha->find(argv[1]);
    std::cerr << argv[1] << " has StringNumber: " << sno << std::endl;
  }
  exit(0);
}
