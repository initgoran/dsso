#ifndef __HUNINFODB_H__
#define __HUNINFODB_H__

#include <map>
#include <set>
#include <vector>
#include <algorithm>

#include <hunspell/hunspell.hxx>

#include "StringDB.h"
#include "StringInfo.h"
#include "InfoDB.h"
#include "WordCountCmp.h"

#define PREFIX_STR "xzwqz"

const char *const huninfodb_filename = "index/huninfodb.bin";

const char *const spell_base_aff = "resources/base.aff";
const char *const spell_base_dic = "resources/base.dic";
const char *const spell_devel_aff = "resources/devel.aff";
const char *const spell_devel_dic = "resources/devel.dic";
const char *const spell_sv_se_aff = "resources/sv_SE.aff";
const char *const spell_sv_se_dic = "resources/sv_SE.dic";

const unsigned char SPELL_DEVEL_OK = 1;
const unsigned char SPELL_BASE_OK = 2;
const unsigned char SPELL_SV_SE_OK = 4;
const unsigned char SPELL_IS_COMPOUND = 8;

const unsigned char SPELL_CAT_MAIN_MASK = 16+32;
const unsigned char SPELL_CAT_ALL_MASK = 16+32+64+128;
const unsigned char SPELL_CAT_OK = 32;
const unsigned char SPELL_CAT_UNDECIDED = 0;
const unsigned char SPELL_CAT_NOK = 16+32;
const unsigned char SPELL_CAT_IGNORE = 16;

const unsigned char SPELL_CAT_OK_MINOR = SPELL_CAT_OK+64;
const unsigned char SPELL_CAT_OK_AMBIGIOUS = SPELL_CAT_OK+128;
const unsigned char SPELL_CAT_OK_MAJOR = SPELL_CAT_OK+64+128;

const unsigned char SPELL_CAT_NOK_SV_FI = SPELL_CAT_NOK+64;
const unsigned char SPELL_CAT_NOK_AMBIGIOUS = SPELL_CAT_NOK+128;
const unsigned char SPELL_CAT_NOK_MAJOR = SPELL_CAT_NOK+64+128;

const unsigned char SPELL_CAT_UNDECIDED_PERHAPS = SPELL_CAT_UNDECIDED+64;
const unsigned char SPELL_CAT_UNDECIDED_AMBIGIOUS = SPELL_CAT_UNDECIDED+128;
const unsigned char SPELL_CAT_UNDECIDED_RARE = SPELL_CAT_UNDECIDED+64+128;

const unsigned char SPELL_CAT_IGNORE_OK = SPELL_CAT_IGNORE+64;
const unsigned char SPELL_CAT_IGNORE_NOK = SPELL_CAT_IGNORE+128;
const unsigned char SPELL_CAT_IGNORE_FORGET = SPELL_CAT_IGNORE+64+128;

static const char *spell_category_name_sorted[16] = {
  "UNDECIDED", "IGNORE", "OK", "NOK",
  "UNDECIDED: Perhaps", "IGNORE: ok", "OK: Not important", "NOK: sv_FI",
  "UNDECIDED: Ambigious", "IGNORE: nok", "OK: Ambigious", "NOK: Ambigious",
  "UNDECIDED: Rare", "IGNORE: Forget", "OK: Important", "NOK: Forbidden"
};
static const char *spell_category_filename_sorted[16] = {
  "index/UNDECIDED.txt", "index/IGNORE.txt", "index/OK.txt", "index/NOK.txt",
  "index/UNDECIDED_Perhaps.txt", "index/IGNORE_ok.txt", "index/OK_Not important.txt", "index/NOK_sv_FI.txt",
  "index/UNDECIDED_Ambigious.txt", "index/IGNORE_nok.txt", "index/OK_Ambigious.txt", "index/NOK_Ambigious.txt",
  "index/UNDECIDED_Rare.txt", "index/IGNORE_Forget.txt", "index/OK_Important.txt", "index/NOK_Forbidden.txt"
};

class Hunspell;

struct A_Dict {
  int no;
  const char *aff, *dic;
  unsigned char mask, unmask;
  Hunspell *dict;
};

extern A_Dict TheDicts[3];
const unsigned int HUNDICT_DEVEL = 0;
const unsigned int HUNDICT_BASE = 1;
const unsigned int HUNDICT_SV_SE = 2;

class HunInfo {
public:
  unsigned char operator()(StringDB::iterator p);
private:
};


class HunInfoDB {
public:
  HunInfoDB();

  void refresh();

  void set_spell_cat(StringDB::iterator wp, unsigned char value) {
    set_spell_cat(wp.string_number(), value);
  }
  void set_spell_cat(unsigned int string_number, unsigned char value) {
    (hdb[string_number] &= ~SPELL_CAT_ALL_MASK) |= value;
  }
  unsigned char get_spell_cat(StringDB::iterator wp) {
    return get_spell_cat(wp.string_number());
  }
  unsigned char get_spell_cat(unsigned int string_number) {
    return (hdb[string_number] & SPELL_CAT_ALL_MASK);
  }
  const char *get_spell_cat_name(unsigned int string_number) {
    return spell_category_name_sorted[get_spell_cat_number(string_number)];
  }
  const char *get_filename_by_spell_cat(unsigned char cat) {
    return spell_category_filename_sorted[cat];
  }

  void dump_spell_categories();

  unsigned char get_spell_cat_number(unsigned int string_number) {
    unsigned char cat = (hdb[string_number] & SPELL_CAT_ALL_MASK) >> 4;
    return cat;
  }
  unsigned char spell_cat_main(StringDB::iterator wp) {
    return spell_cat_main(wp.string_number());
  }
  unsigned char spell_cat_main(unsigned int string_number) {
    return (hdb[string_number] & SPELL_CAT_MAIN_MASK);
  }
  
  void freqsort(std::vector<unsigned int> &v) {
    std::sort(v.begin(), v.end(), wc_cmp);
  }
  unsigned char spell_status(StringDB::iterator wp) {
    return spell_status(wp.string_number());
  }
  unsigned char spell_status(unsigned int string_number) {
    //std::cerr << "checking " << string_number << std::endl;
    return hdb[string_number];
  }

  bool spell_ok(unsigned int string_number) {
    return (hdb[string_number] & SPELL_DEVEL_OK);
  }
  bool spell_ok(StringDB::iterator wp) {
    return spell_ok(wp.string_number());
  }
  bool spell_compound(unsigned int string_number) {
    return (hdb[string_number] & SPELL_IS_COMPOUND);
  }
  bool spell_compound(StringDB::iterator wp) {
    return spell_compound(wp.string_number());
  }

  void suggest(const char *word, std::vector<std::string> &v) {
    char **strlist = 0;
    int n = TheDicts[0].dict->suggest(&strlist, word);
    for (int i=0; i<n; ++i)
      v.push_back(strlist[i]);
    TheDicts[0].dict->free_list(&strlist, n);
  }
  
  int check_word(const char *word) {
    int infobits;
    if (TheDicts[0].dict->spell(word, &infobits, (char **)0)) {
      if (infobits & SPELL_COMPOUND)
	return 2;
      else
	return 1;
    } else
      return 0;
  }

  bool is_a_prefix(const char *prefix) {
    std::string word(prefix);
    word += "yzwzq";
    return TheDicts[0].dict->spell(word.c_str());
  }

  bool is_a_prefix(std::string word) {
    word += "yzwzq";
    return TheDicts[0].dict->spell(word.c_str());    
  }

  bool is_a_suffix(const char *w) {
    strncpy(prefix_check_str, w, _max_wlen);
    return TheDicts[0].dict->spell(prefix_check_buf);
  }

  bool is_a_suffix(std::string word) {
    return is_a_suffix(word.c_str());
  }

  // Debug function, will study elements 2,3,... of an
  // argv-like vector.
  unsigned int find_prefixes(std::string word, WCMAP &suffix_cache, WCMAP &prefixes);

  void prefix_analysis(std::string prefix, WCMAP &wc);

  void suffix_analysis(WCMAP &suffixes, WCMAP &wc1, WCMAP &wc2, WCMAP &wc3);

  // Word that is not currently a suffix; return the score
  // it would get as a suffix:
  unsigned long suffix_score(const char *word);

  // Word that is a valid suffix; return the number of compound words
  // with that suffix:
  unsigned long valid_suffix_score(const char *word, bool explain = false);

  // Word that is not currently a suffix; return the score
  // it would get as a suffix:
  unsigned long _prefix_score(const char *word);
  unsigned long prefix_score(const char *word) {
    if (is_a_prefix(word))
      return 0;
    return _prefix_score(word);
  }
  unsigned long prefix_score(std::string word) {
    if (is_a_prefix(word))
      return 0;
    return _prefix_score(word.c_str());
  }

  // Word that is a valid suffix; return the number of compound words
  // with that suffix:
  unsigned long valid_prefix_score(const char *word) {
    if (is_a_prefix(word))
      return _valid_prefix_score(word);
    return 0;
  }
  unsigned long valid_prefix_score(std::string word) {
    if (is_a_prefix(word))
      return _valid_prefix_score(word.c_str());
    return 0;
  }

  unsigned long _valid_prefix_score(const char *word);

  // Return value >= 0 if currently not a valid prefix,
  // the value is number of currently "red" words that contain this prefix.
  // Negative value (<0) if it is a valid prefix, one less than minus the
  // number of compound words with this prefix.
  long signed_prefix_score(std::string word);

  void ends_with(const char *word, bool spell_ok, std::vector<unsigned int> &res);

  void starts_with(const char *word, bool spell_ok, std::vector<unsigned int> &res);

  void reload_dict(A_Dict &adict, StringDB::iterator from, StringDB::iterator to,
		   bool print_self_diff = false, int other_diff = -1);
  void reload_devel();
  void reload_base();
  void reload_sv_se();
  static const unsigned char spell_category_number[16];
  static const char *spell_category_name[16];
 private:
  static const size_t _max_wlen = 100;
  char prefix_check_buf[_max_wlen + strlen(PREFIX_STR) + 1];
  char *prefix_check_str;
  std::map<std::string, long> prefix_score_cache;
  void clear_cache() {
    prefix_score_cache.clear();
  }
  InfoDB<unsigned char, HunInfo> &hdb;
  InfoDB<StringInfo, EmptyStringInfo> &infodb;
  StringDB &sdb;
  WordCountCmp wc_cmp;    
};

#endif
