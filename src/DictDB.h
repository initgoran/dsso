#ifndef __DICTDB_H__
#define __DICTDB_H__

#include <iostream>
#include <string>
#include <map>
#include <vector>

#include "SyU.h"
#include "RawStringIndex.h"
#include "SyuGetWordforms.h"
#include "StringDB.h"

// For the given IC, replace re (at end) with suffix.
// E.g. replace "a$" with "orna".
struct ic_pattern {
  ic_pattern(unsigned int i, std::string r, std::string s)
  : icno(i),
    re(r),
    suffix(s) {
  }
  unsigned int icno;
  std::string re;
  std::string suffix;
};

unsigned int get_json_id_val(const char *);

class DictDB {
 public:
  DictDB(const std::string &db_file, const std::string &index_file,
	 const std::string &journal_file, const std::string &wf_index_file, unsigned int (*fp)(const char *) = 0)
    : _rsdb(db_file, index_file, journal_file, fp),
    csdb(StringDB::get_db()),
    idx(_rsdb, wf_index_file)
    {
      //std::cerr << "Init grammar..." << std::endl;
      init_grammar();
    }
  void refresh() {
    //std::cerr << "DictDB: saving index...\n";
    idx.save_cache();
  }
  void refresh_cache() {
    idx.update();
  }
  ~DictDB() {
    refresh();
  }
  std::set<unsigned int> search_wf(unsigned int sno) {
    return idx.find_ids(sno);
  }
  unsigned int next_id() {
    return _rsdb.next_id();
  }
  void syu_score(WCMAP &wfscore, std::map<unsigned int, unsigned long> &syuscore);
  std::set<unsigned int> search_wf(const char *word) {
    unsigned int sno = csdb.find(word);
    if (!sno)
      return std::set<unsigned int>();
    return idx.find_ids(sno);
  }
  SyU *save(SyU *syu) {
    unsigned int syuid = syu->id();
    if (!syuid)
      syuid = next_id();
    //std::cerr << "DictDB save: " << syu->show_full() << std::endl;
    std::string str = syu->save(syuid);
    //std::cerr << "DictDB save <" << str << ">\n";
    _rsdb.set_string(syuid, str.c_str());
    delete syu;
    syu = new SyU(str.c_str());
    object_cache[syuid] = syu;
    idx.update();
    return syu;
  }
  SyU *get_syu(unsigned int id);

  SyU *create_syu(std::string word, std::string gc) {
    return SyU::create(word, gc, (*_gc2ic[gc])[0]);
  }

  void spell_sets(SpellSets &dicts, std::string exclude_tag_list =
		  "sv_FI,kontrollera,redundant,ovanlig");

  void build_hunspell(std::string dirname);
  void build_hunspell_wordlists(std::string dirname, const char *tags);
  void build_hunspell_make(std::string dirname, const char *target);
  void build_fi_hunspell(std::string dirname);

  void dump_old_format(std::string filename);

  SyU *create_syu(std::string word, unsigned int gcno) {
    return create_syu(word, _gcs[gcno]);
  }

  const std::vector<int> *ics(const std::string &gc) {
    return _gc2ic[gc];
  }

  const std::map<std::string, std::vector<int> *> &gc2ic() const {
    return _gc2ic;
  }
  const std::vector<std::string> &ics() const {
    return _ics;
  }
  const std::vector<std::string> &gcs() const {
    return _gcs;
  }
  unsigned int gcno(std::string gc) {
    return _gc2gcno[gc];
  }
  const std::string &ic(unsigned int icno) {
    return _ics[icno];
  }

  class iterator {
  public:
  iterator(DictDB *dictdb, unsigned int syuid = 1) : ddb(dictdb), curr_pos(syuid), _end(ddb->next_id()) {}
    // iterator(const iterator&);
    //~iterator();

    iterator& operator=(const iterator &rhs) {
      curr_pos = rhs.curr_pos;
      ddb = rhs.ddb;
      return *this;
    }

    bool operator==(const iterator &rhs) const {
      return (curr_pos == rhs.curr_pos and ddb == rhs.ddb);
    }
    bool operator!=(const iterator &rhs) const {
      return (curr_pos != rhs.curr_pos or ddb != rhs.ddb);
    }
    iterator &operator++() {
      while (curr_pos < _end) {
	++curr_pos;
	SyU *syu = ddb->get_syu(curr_pos);
	if (syu != 0 and syu->type() == "syu")
	  break;
      }
      return *this;
    }
    iterator operator++(int) {
      iterator it(*this);
      ++(*this);
      return it;
    }
    iterator& operator--() {
      SyU *syu;
      do {
	--curr_pos;
	syu = ddb->get_syu(curr_pos);
	if (syu != 0 and syu->type() == "syu")
	  break;
      } while ( curr_pos>1 and (syu == 0 or syu->type() != "syu"));
      return *this;
    }
    iterator operator--(int) {
      iterator it(*this);
      --(*this);
      return it;
    }
    SyU *operator*() const {
      return ddb->get_syu(curr_pos);
    }
    unsigned int syuid() {
      return curr_pos;
    }
    //pointer operator->() const;
  private:
    DictDB *ddb;
    unsigned int curr_pos;
    unsigned int _end;
  };
  iterator begin() {
    return iterator(this);
  }
  iterator end() {
    return iterator(this, _rsdb.next_id());
  }

  std::vector<ic_pattern> *pattern(std::string pattern_name);
  const std::vector<std::string> &patterns(std::string word, std::string gc);
  void apply_pattern(SyU *syu, std::string pname);

  // Returns a vector with IDs of SyUs that may be suffixes to the given compound word
  const std::vector<SyU *> &possible_suffixes(std::string compound);

 private:
  void init_grammar();
  RawStringDB _rsdb;
  StringDB &csdb;
  RawStringIndex<unsigned int, SyuGetWords> idx;
  Json::Value grammar;
  SyU *syu_grammar;

  // Map object ID to object:
  static std::map<unsigned int, SyU *> object_cache;

  // Map pattern name to list of substitutions:
  std::map<std::string, std::vector<ic_pattern> *> pattern_cache;

  // Map GC name to list of ICs:
  static std::map<std::string, std::vector<int> *> _gc2ic;

  // Map GC name to vector of pattern name+condition:
  std::map<std::string, std::vector<std::pair<std::string, std::string> > *> _gc2plist;

  // Map pattern name to list of substitutions
  std::map<std::string, std::vector<ic_pattern> *> _ptn2slist;
  static std::map<std::string, unsigned int> _gc2gcno;
  static std::vector<std::string> _gcs;
  static std::vector<std::string> _ics;
};

#endif
