//#include <algorithm>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>


#include "StringInfo.h"
#include "StringDB.h"

#include <iostream>

int main(int argc, char *argv[]) {
  int count[MAX_WORD_LENGTH/StrLength+1];
  int length[MAX_WORD_LENGTH];
  for (unsigned int i=0; i<sizeof(count)/sizeof(int); ++i) {
    count[i]=0;
  }
  for (unsigned int i=0; i<sizeof(length)/sizeof(int); ++i) {
    length[i]=0;
  }

  StringDB sdb = StringDB::get_db();
  StringDB::iterator p = sdb.begin(), old(p);
  while (old != sdb.end()) {
    ++length[strlen(*p)];
    ++p;
    ++count[p.string_number()-old.string_number()];
    old = p;
  }
  std::cerr << "Word byte-length / count:\n";
  for (unsigned int i=1; i<sizeof(length)/sizeof(int); ++i) {
    std::cerr << i << ' ' << length[i] << std::endl;
  }
  std::cerr << "Block size / count:\n";
  for (unsigned int i=1; i<sizeof(count)/sizeof(int); ++i) {
    std::cerr << i << ' ' << count[i] << std::endl;
  }
}
