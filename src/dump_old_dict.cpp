#include <iostream>
#include <string>

#include "DictDB.h"

int main(int argc, char *argv[]) {

  std::string grammar = "{\"meta\":{\"max_id\":1287048,\"server\":\"dsso.se\",\"contact\":\"initgoran@gmail.com\"},\"ordklass\":[\n{\"id\":\"1\",\"rev\":1,\"name\":\"substantiv\",\"ics\":[\"obestämd form singularis\",\"obestämd form singularis, genitiv\",\"bestämd form singularis\",\"bestämd form singularis, genitiv\",\"obestämd form pluralis\",\"obestämd form pluralis, genitiv\",\"bestämd form pluralis\",\"bestämd form pluralis, genitiv\"]},\n{\"id\":\"2\",\"rev\":1,\"name\":\"verb\",\"ics\":[\"infinitiv\",\"preteritum\",\"supinum\",\"presens\",\"imperativ\",\"infinitiv, passiv form\",\"preteritum, passiv form\",\"supinum, passiv form\",\"presens, passiv form\",\"perfekt particip, utrum\",\"perfekt particip, neutrum\",\"perfekt particip, plural\",\"presens particip \",\"konjunktiv\"]},\n{\"id\":\"16\",\"rev\":1,\"name\":\"presens particip\",\"ics\":[\"presens particip\"]},\n{\"id\":\"17\",\"rev\":1,\"name\":\"perfekt particip\",\"ics\":[\"perfekt particip, utrum\",\"perfekt particip, neutrum\",\"perfekt particip, plural\"]},\n{\"id\":\"3\",\"rev\":1,\"name\":\"adjektiv\",\"ics\":[\"positiv utrum\",\"positiv neutrum\",\"bestämd form\",\"pluralis\",\"komparativ\",\"superlativ\",\"superlativ, bestämd form\",\"maskulin\",\"positiv utrum, genitiv\",\"positiv neutrum, genitiv\",\"bestämd form, genitiv\",\"pluralis, genitiv\",\"komparativ, genitiv\",\"superlativ, genitiv\",\"superlativ, bestämd form, genitiv\",\"maskulin, genitiv\"]},\n{\"id\":\"4\",\"rev\":1,\"name\":\"egennamn\",\"ics\":[\"egennamn\",\"egennamn, genitiv\"]},\n{\"id\":\"5\",\"rev\":1,\"name\":\"adverb\",\"ics\":[\"adverb\"]},\n{\"id\":\"6\",\"rev\":1,\"name\":\"räkneord\",\"ics\":[\"räkneord\"]},\n{\"id\":\"7\",\"rev\":1,\"name\":\"deponens\",\"ics\":[\"infinitiv\",\"preteritum\",\"supinum\",\"presens\",\"imperativ\"]},\n{\"id\":\"8\",\"rev\":1,\"name\":\"förkortning\",\"ics\":[\"förkortning\"]},\n{\"id\":\"9\",\"rev\":1,\"name\":\"prefix\",\"ics\":[\"prefix\"]},\n{\"id\":\"10\",\"rev\":1,\"name\":\"pronomen\",\"ics\":[\"pronomen\"]},\n{\"id\":\"11\",\"rev\":1,\"name\":\"interjektion\",\"ics\":[\"interjektion\"]},\n{\"id\":\"12\",\"rev\":1,\"name\":\"preposition\",\"ics\":[\"preposition\"]},\n{\"id\":\"13\",\"rev\":1,\"name\":\"konjunktion\",\"ics\":[\"konjunktion\"]},\n{\"id\":\"14\",\"rev\":1,\"name\":\"subjunktion\",\"ics\":[\"subjunktion\"]},\n{\"id\":\"15\",\"rev\":1,\"name\":\"infinitivmärke\",\"ics\":[\"infinitivmärke\"]}\n],\"syntaktisk_enhet\":[\n";

  std::cout << grammar;
  
  DictDB dssodb("dsso_db.txt", "dsso_db.bin", "dsso_db.journal", "dssodb_word_index.txt");
  DictDB::iterator syu = dssodb.begin();
  std::cout << (*syu)->old_format();
  ++syu;
  for (; syu != dssodb.end(); ++syu) {
    std::cout << ",\n" << (*syu)->old_format();
  }
  std::cout << "\n]}";
}
