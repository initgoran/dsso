#include <fstream>
#include <functional>
#include <utility>
#include <exception>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <glibmm/ustring.h>

#include <string>

#include "HunInfoDB.h"
#include "JunkDetector.h"

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] << " infilename outfilename" << std::endl;
    exit(1);
  }
  std::ifstream fd(argv[1]);
  std::ofstream out_fd(argv[2]);
  if (!fd or !out_fd) {
    std::cerr << "Cannot open " << argv[1] << " or " << argv[2] << ": " << strerror(errno) << std::endl;
    exit(1);
  }
  HunInfoDB *huninfodb = new HunInfoDB();
  Detector detector(huninfodb);
  std::string line;

  while ( getline(fd, line) ) {
    if (detector.check_line(line))
      out_fd << line << std::endl;
  }
  out_fd.close();
  fd.close();
  exit(0);
}
