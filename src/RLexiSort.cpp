#include "StringDB.h"

#include <iostream>
#include <fstream>
#include <exception>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include "StringSort.h"

int main(int argc, char *argv[]) {

  StringDB &sdb = StringDB::get_db();

  StringSort<RLexiCmp> *beta = StringSort<RLexiCmp>::get_db("index/rlexisort.txt");
  //beta->refresh();

  {
    std::ofstream out("rwordlist.txt");
    StringSort<RLexiCmp>::iterator p = beta->begin();
    while (p != beta->end())
      out << sdb[*p++] << std::endl;
    out.close();
  }

  if (argc < 2)
    exit(0);

  const char *word = argv[1];
  unsigned int len = strlen(word);
  StringSort<RLexiCmp>::iterator p = beta->find_first(word);
  if (p != beta->end()) {
    /*
    std::cerr << word << " is before StringNumber: " << *p
	      << " string <" << sdb[*p] << '>' << std::endl;
    */
    while (strcmp(word, sdb[*p]+strlen(sdb[*p])-len) == 0) {
      std::cerr << sdb[*p++] << std::endl;
    }
	   
  }
  exit(0);
}
