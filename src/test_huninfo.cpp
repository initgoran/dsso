#include <string.h>
#include <iostream>
#include <fstream>
#include "HunInfoDB.h"
#include "StringInfo.h"
#include "StringSort.h"
#include "StringDB.h"
#include "InfoDB.h"
#include <glib.h>
#include <locale>

int main() {
  setlocale(LC_ALL, "sv_SE.UTF-8");
  std::string s = "Örjan";
  Glib::ustring f(s);
  gunichar c = f[0];
  gunichar d = g_unichar_toupper(c);
  std::cerr << c << " " << d << "\n";
  if (g_unichar_isupper(c)) {
    std::cerr << s << " Upper\n";
  } else {
    std::cerr << s << " Lower\n" << d + f.substr(1) << "\n";
  }

  exit(0);

  HunInfoDB *huninfodb = new HunInfoDB();
  WCMAP suffixes;


  huninfodb->prefix_analysis("boknings", suffixes);
  for (WCMAP::iterator it=suffixes.begin(); it!=suffixes.end(); ++it)
    std::cerr << it->first << " " << it->second << std::endl;
  exit(0);

  InfoDB<StringInfo, EmptyStringInfo> *infodb = InfoDB<StringInfo, EmptyStringInfo>::get_dbp("index/stringinfo.bin");


  /*
  std::ifstream words("alist.txt");
  
  for (std::string word; words >> word; ) {
    int sp_res = huninfodb->check_word(word.c_str());
    if (sp_res == 0) {
      std::cout << word << std::endl;
    } else {
      std::cerr << word << std::endl;
    }
  }
  */

  WCMAP wfs, prefs, nonprefs, vprefs;
  wfs["ragu"]=0;
  wfs["ragus"]=0;
  wfs["ragun"]=0;
  wfs["raguns"]=0;
  wfs["raguer"]=0;
  wfs["raguers"]=0;
  wfs["raguerna"]=0;
  wfs["raguernas"]=0;
  
  huninfodb->suffix_analysis(wfs, prefs, nonprefs, vprefs);

  for (WCMAP::const_iterator it=wfs.begin(); it != wfs.end(); ++it) {
    std::cout << it->first << ": score=" << it->second << "\n";
  }
  for (WCMAP::const_iterator it=prefs.begin(); it != prefs.end(); ++it) {
    std::cout << it->first << ": prefscore=" << it->second << "\n";
  }
  for (WCMAP::const_iterator it=nonprefs.begin(); it != nonprefs.end(); ++it) {
    std::cout << it->first << ": nonprefscore=" << it->second << "\n";
  }
  for (WCMAP::const_iterator it=vprefs.begin(); it != nonprefs.end(); ++it) {
    std::cout << it->first << ": vprefscore=" << it->second << "\n";
  }

  RCMAP sprefs = flip_map(prefs);
  for (RCMAP::const_iterator it=sprefs.begin(); it != sprefs.end(); ++it) {
    std::cout << it->second << ": sprefscore=" << it->first << "\n";
  }
  
  //const char *wfs[7] = { "", "", "ragu", "ragus", "ragun", "raguns", NULL };
  //huninfodb->find_prefixes(wfs);
  
  delete huninfodb;
  std::cout << "" << "\n";
}
