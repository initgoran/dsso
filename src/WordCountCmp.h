#ifndef __WORDCOUNTCMP_H__
#define __WORDCOUNTCMP_H__

#include "StringInfo.h"
#include "InfoDB.h"

struct WordCountCmp {
  InfoDB<StringInfo, EmptyStringInfo> &infodb;
  WordCountCmp(InfoDB<StringInfo, EmptyStringInfo> &idb) : infodb(idb) {}
  bool operator()(unsigned int left, unsigned int right) const {
    if (infodb.get_info(left).word_count != infodb.get_info(right).word_count)
      return (infodb.get_info(left).word_count < infodb.get_info(right).word_count);
    return left < right;
  }
};


#endif
