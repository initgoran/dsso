#include <string.h>
#include <fstream>
#include <set>

#include "StringDB.h"
#include "StringInfo.h"
#include "FreqCmp.h"
#include "HunInfoDB.h"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "usage: " << argv[0] << " filename" << std::endl;
    exit(1);
  }
  StringDB &sdb = StringDB::get_db();
  HunInfoDB hdb;
  InfoDB<StringInfo, EmptyStringInfo> &infodb = InfoDB<StringInfo, EmptyStringInfo>::get_db("index/stringinfo.bin");
  hdb.refresh();
  std::ifstream fd(argv[1]);
  if (!fd) {
    std::cerr << "cannot open " << argv[1] << ": " << strerror(errno) << std::endl;
    exit(1);
  }
  while (fd) {
    std::string line;
    getline(fd, line);
    std::istringstream lfd(line);
    std::ostringstream res;
    std::string word;
    unsigned long score = 0;
    std::set<unsigned int> s;
    while (lfd >> word) {
      unsigned int sno = sdb.find(word);
      if (!sno)
	continue;
      if (s.find(sno) != s.end())
	continue;
      s.insert(sno);
      unsigned char cat = hdb.spell_cat_main(sno);
      if (cat == SPELL_CAT_IGNORE or cat == SPELL_CAT_NOK)
	continue;
      if (!hdb.spell_ok(sno)) {
	unsigned long c = infodb.get_info(sno).word_count;
	score += c;
	if (c)
	  res << word << ' ' << c << ' ';
      }
    }
    if (score > 100) {
      std::cout << score << ' ' << res.str() << "% " << line << std::endl;
    }
  }
}
